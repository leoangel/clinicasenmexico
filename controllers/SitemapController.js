var express = require('express');
var app = express();
var fs = require('fs');
var dao = require('../dao/SitemapDAO');
var errorLog = require('../library/util/errorlog');
var config = require('../config/app');

app.set('views', config.view_path);
app.set('view engine', config.view_engine);

var SitemapController = {

    url: 'https://clinicasyhospitales.com.co',

    /**
     * 
     * @param {type} req
     * @param {type} res
     * @param {type} next
     * @returns {undefined}
     */
    generarServicios: function (req, res, next) {

        dao.getAllServices().then(function (data) {
            var urlsTotal = [];
            data.forEach(function (s) {
                urlsTotal.push({
                    url: SitemapController.url + '/especialidad/' + s.serv_url,
                });
            });
            delete data;

            var urlslength = urlsTotal.length;

            let fileName = 'servicios.xml';
            let pathFile = __dirname + '/../public/sitemaps/' + fileName;

            app.render('sitemap/index', { urls: urlsTotal }, function (err, html) {
                fs.writeFileSync(pathFile, html, {
                    encoding: 'utf8'
                });
            });

            res.send('Urls: ' + urlslength);
        });
    },

    generarRegiones: function (req, res, next) {

        dao.getAllRegiones().then(function (data) {
            var urlsTotal = [];
            data.forEach(function (s) {
                urlsTotal.push({
                    url: SitemapController.url + '/' + s.url,
                });
            });
            delete data;

            var urlslength = urlsTotal.length;

            let fileName = 'regiones.xml';
            let pathFile = __dirname + '/../public/sitemaps/' + fileName;

            app.render('sitemap/index', { urls: urlsTotal }, function (err, html) {
                fs.writeFileSync(pathFile, html, {
                    encoding: 'utf8'
                });
            });

            res.send('Urls: ' + urlslength);
        });
    },

    generarRegionesServicios: function (req, res, next) {

        dao.getAllRegionesServicios().then(function (data) {
            var urlsTotal = [];
            data.forEach(function (s) {
                urlsTotal.push({
                    url: SitemapController.url + '/' + s.reg_url + '/' + s.serv_url,
                });
            });
            delete data;

            var sitemapIndex = [];
            var countSitemaps = 1;
            var urlslength = urlsTotal.length;

            var urlsC = chunk(urlsTotal, 50000);
            delete urlsTotal;

            urlsC.forEach(function (urls, index) {

                let fileName = 'region-servicios-' + countSitemaps++ + '.xml';
                let pathFile = __dirname + '/../public/sitemaps/' + fileName;

                sitemapIndex.push({
                    url: SitemapController.url + '/sitemaps/' + fileName
                });

                app.render('sitemap/index', { urls: urls }, function (err, html) {
                    fs.writeFileSync(pathFile, html, {
                        encoding: 'utf8'
                    });
                });

                // Generar el sitemaps index
                if (urlsC[index + 1] == undefined) {
                    let pathFileS = __dirname + '/../public/sitemaps/regiones-servicios-idx.xml';
                    app.render('sitemap/indexes', { urls: sitemapIndex, lastMod: getDate() }, function (err, html) {
                        fs.writeFileSync(pathFileS, html, {
                            encoding: 'utf8'
                        });
                    });
                }

            });

            res.send('Urls: ' + urlslength);

        });
    },
    /**
     * 
     * @param {type} req
     * @param {type} res
     * @param {type} next
     * @returns {undefined}
     */
    generarEstablecimientos: function (req, res, next) {

        var sitemapIndex = [];
        var countSitemaps = 1;

        dao.getAllEstablecimientos().then(function (data) {

            var count = data.length;
            var urlsC = chunk(data, 50000);

            urlsC.forEach(function (datap, index) {

                var urls = [];
                datap.forEach(function (estab) {
                    urls.push({
                        url: SitemapController.url + '/ips/' + estab.nombre_url
                    });
                });

                var fileName = 'estab-' + countSitemaps++ + '.xml';
                var pathFile = __dirname + '/../public/sitemaps/estab/' + fileName;

                app.render('sitemap/index', { urls: urls }, function (err, html) {
                    fs.writeFileSync(pathFile, html, {
                        encoding: 'utf8'
                    });
                });

                sitemapIndex.push({
                    url: SitemapController.url + '/sitemaps/estab/' + fileName,
                    lastMod: getDate()
                });

                // Generar el sitemaps index
                if (urlsC[index + 1] == undefined) {
                    let pathFileS = __dirname + '/../public/sitemaps/estab-idx.xml';
                    app.render('sitemap/indexes', { urls: sitemapIndex, lastMod: getDate() }, function (err, html) {
                        fs.writeFileSync(pathFileS, html, {
                            encoding: 'utf8'
                        });
                    });
                }
            });

            res.send('Iniciando ... ' + ' Urls: ' + count);
        });
    },
}

function getDate() {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }

    return year + '-' + month + '-' + day;
}

function chunk(arr, size) {
    var tempAr = [];
    var j = 0;
    for (var i = 0; i < arr.length; i++) {
        if (j == size || j == 0) {
            tempAr.push(arr.slice(i, (i + size)));
            j = 0;
        }
        j++;
    }
    return tempAr;
}

module.exports = SitemapController;