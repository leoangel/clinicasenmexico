var dao = require('../dao/SearchDAO');
var utilFncs = require('../library/util/UtilFncs');

var Search = {

    /**
     * Full text search
     */
    buscar: function (req, res) {

        var util = utilFncs.init(req, res);
        let q = util.getReqQuery('q');
        let s = util.getReqQuery('s');
        let l = util.getReqQuery('l');

        let countSearch = 0;
        let searchBD = function (q, r = false) {
            dao.find(q, r).then(function (results) {
                // Si no arroja resultados volver a buscar modificando la busqueda
                if (results.length == 0 && countSearch++ == 0) {
                    searchBD(util.getFirstWord(q), true);
                } else
                    view(results);

            }, function (err, origen) {
                util.fncError(err, origen);
                view();
            });
        }

        let view = function (results = []) {

            // Agrega urls que se usaran para mostrar en los resultados
            results.forEach(function (r, i) {
                if (r.tipo == 'e') {
                    results[i].url_tipo = util.getBaseUrl() + '/ips/' + r.url;
                }
                if (r.tipo == 'l') {
                    results[i].url_tipo = util.getBaseUrl() + '/' + r.url;
                }
                if (r.tipo == 's') {
                    results[i].url_tipo = util.getBaseUrl() + '/especialidad/' + r.url;
                }
                if (r.tipo == 'sl') {
                    results[i].url_tipo = util.getBaseUrl() + '/' + r.url;
                }
                if (r.tipo == 'q') {
                    let pre_path = r.pre_path_url || util.getCustomQueriesPath();
                    results[i].url_tipo = util.getBaseUrl() + '/' + pre_path + '/' + r.url;
                }
            });

            util.render('search', {
                q: dao.initcap(q),
                results: results
            });
        }

        if (util.isEmpty(q)) {
            dao.customQueriesSearch().then(function (results) {
                results.forEach((data, i) => {
                    results[i].url_tipo = util.getBaseUrl() + '/' + util.getCustomQueriesPath() + '/' + data.url;
                });
                view(results)
            }, function (err, origen) {
                util.errorSend404(err, origen)
            });
        } else {
            searchBD(q);
            // TODO 
            // guardar la consulta
        }
    },

    /**************************************************************************/
    /**
     * Autocompletar
     */
    jsonAutocompletar: function (req, res) {

        var util = utilFncs.init(req, res);

        /**
         * Identifica si solo se ha ingresado 1 caracter para buscar por inicial sino se busca por subtring
         */
        var consultarSugerencias = function (textSearch, tipo, q = true) {

            let query = (q) ? (util.isInitial(textSearch)) ? 'ByInitial' : 'BySub' : '';

            dao['find' + tipo + query](textSearch).then(function (data) {
                res.json(data);
            }, function (err, origen, code) {
                if (code !== 404)
                    util.fncError(err, origen);
                res.json({});
            });
        }

        /**
         * 1 Servicio, Nombre establecimiento
         * 2 Ubicacion
         */
        const type = util.getReqQuery('t');
        const textSearch = util.getReqQuery('v');

        if (!textSearch || ['1', '2', '3', '10', '11', '30'].indexOf(type) == -1) {
            return res.json({});
        }

        if (type == '1') {
            consultarSugerencias(textSearch, 'Service');
        }
        if (type == '2') {
            consultarSugerencias(textSearch, 'Establishment');
        }
        if (type == '3') {
            consultarSugerencias(textSearch, 'Location');
        }
        if (type == '10') {
            consultarSugerencias(textSearch, 'ServiceLevenshtein', false);
        }
        if (type == '11') {
            consultarSugerencias(textSearch, 'AllServices', false);
        }
        if (type == '30') {
            consultarSugerencias(textSearch, 'LocationLevenshtein', false);
        }
    },

    jsonRegionesPorServicio: function (req, res) {

        var util = utilFncs.init(req, res);

        /**
         * Identifica si solo se ha ingresado 1 caracter para buscar por inicial sino se busca por subtring
         */
        let q = util.getReqQuery('search');
        if (!q)
            return res.json({});

        let query = (util.isInitial(q)) ? 'ByInitial' : 'BySub';

        dao['findRegionesByServicio' + query](req.params.servicio, q).then(function (data) {
            res.json(data);
        }, function (err, origen, code) {
            if (code !== 404)
                util.fncError(err, origen);
            res.json({});
        });
    }

}

module.exports = Search;


