var dao = require('../dao/DirectorioDAO');
var utilFncs = require('../library/util/UtilFncs');

var Directorio = {

    /**************************************************************************/
    /**
     * Home
     */
    getHome: function (req, res) {

        var util = utilFncs.init(req, res);
        dao.findPopulares().then(function (data) {

            let consultas = data.slice(0,9);
            let servicios = data.slice(9);

            util.render('home', {
                consultas: consultas,
                servicios: servicios
            }, false);
        }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },

    /**
     * Identifica si la peticion es a un establecimiento unico o 
     * a un establecimiento que tiene muchas sedes
     */
    getEstablecimientoODirectorio: function (req, res) {

        var util = utilFncs.init(req, res);
        dao.findEstablecimiento(req.params.ips).then(
                function (est, dir) {
                    // Si hay de una ips con el mismo nombre
                    if (est.nombre_duplicado == 1 && est.nombre_sede_url != req.params.ips) {
                        // Mostrar directorio
                        getDirEstablecimiento(est, dir, util);
                    } else {
                        // Mostrar detalle
                        getEstablecimiento(est, util);
                    }
                },
                function (err, origen) {
                    util.errorSend404(err, origen);
                }
        );

        /**
         * Establecimiento
         */
        var getEstablecimiento = function (est, util) {

            est.numero_sede_principal = est.numero_sede_principal || 0;
            var principal = (parseInt(est.numero_sede) === parseInt(est.numero_sede_principal)) ? true : false;
            var sede_info = (est.nombre_duplicado) ? {title: est.nombre_sede_title, url: est.nombre_sede_url} : {title: est.nombre, url: est.nombre_url};

            util.render('establecimiento', {
                info: est,
                principal: principal,
                sedeu: sede_info
            });
        }
        /**
         * Establecimiento directorio
         */
        var getDirEstablecimiento = function (est, data, util) {
            util.render('establecimiento-dir', {
                info: est,
                data: data
            });
        }
    },

    /**************************************************************************/
    /**
     * Establecimiento Servicio
     */
    getEstabServicio: function (req, res) {

        var util = utilFncs.init(req, res);
        dao.findEstablecimientoServicio(req.params.ips, req.params.servicio).then(
                function (results) {

                    var servicios = [];
                    results.forEach(function (row) {
                        servicios.push({
                            grse_nombre: row.grse_nombre,
                            hospitalario: row.hospitalario,
                            unidad_movil: row.unidad_movil,
                            domiciliario: row.domiciliario,
                            complejidad_baja: row.complejidad_baja,
                            complejidad_media: row.complejidad_media,
                            complejidad_alta: row.complejidad_alta
                        });
                    });

                    var info = results[0];
                    var sede_info = (info.nombre_duplicado) ? {title: info.nombre_sede_title, url: info.nombre_sede_url} : {title: info.sede_nombre, url: info.sede_nombre_url};

                    util.render('establecimiento-servicio', {
                        servicios: servicios,
                        info: info,
                        sedeu: sede_info
                    });
                }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    getServicios: function (req, res, next) {

        var util = utilFncs.init(req, res);
        dao.getServicios().then(function (servicios) {
            util.render('servicios', {
                servicios: servicios
            });
        }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    getServicioRegiones: function (req, res, next) {

        var util = utilFncs.init(req, res);
        dao.findRegionesPorServicio(req.params.servicio).then(function (regiones) {
            util.render('servicio-regiones', {
                regiones: regiones,
                servicio: regiones[0],
                count: regiones.length
            });
        }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    getRegiones: function (req, res, next) {

        var util = utilFncs.init(req, res);
        var l = util.getReqQuery('i') || '';
        var alphabet = [...'ABCDEFGHIJKLMNOPQRSTUVWXYZ'];

        dao.getRegiones(l).then(function (regiones) {
            util.render('regiones', {
                regiones: regiones,
                alphabet: alphabet,
                canonical: l
            });
        }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    /**
     * Region Pages
     */
    getEstPorRegion: function (req, res, next) {

        var util = utilFncs.init(req, res);
        dao.findRegionSubregionPage(req.params.region, req.params.page).then(
                function (rows, count, region, page, pages) {
                    util.render('region-page', {
                        region: region,
                        data: rows,
                        currentUrl: '/' + region.url,
                        pageData: util.getInfoPage(pages, page),
                    });
                }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**
     * Servicios disponibles en la region
     */
    getServiciosPorRegion: function (req, res) {

        var util = utilFncs.init(req, res);
        dao.findServiciosByRegion(req.params.region).then(
                function (servicios) {
                    util.render('region-servicios', {
                        servicios: servicios,
                        region: servicios[0],
                        count: servicios.length
                    });
                }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    /**
     * Servicio
     */
    getServicioPage: function (req, res, next) {

        var util = utilFncs.init(req, res);
        dao.findServicioPage(req.params.servicio, req.params.page).then(
                function (rows, count, servicio, page, pages) {
                    util.render('servicio-page', {
                        servicio: servicio,
                        data: rows,
                        currentUrl: '/especialidad/' + servicio.serv_url,
                        pageData: util.getInfoPage(pages, page),
                    });
                }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },
    /**************************************************************************/
    getEstPorRegionServicio: function (req, res, next) {

        var util = utilFncs.init(req, res);
        dao.findPorServicioRegion(req.params.servicio, req.params.region, req.params.page).then(
                function (rows, count, servicio, page, pages) {
                    util.render('servicio-region-page', {
                        servicio: servicio,
                        data: rows,
                        currentUrl: '/' + servicio.ubi_url + '/' + servicio.serv_url,
                        pageData: util.getInfoPage(pages, page),
                    });
                }, function (err, origen) {
            util.errorSend404(err, origen);
        });
    },

    /**************************************************************************/
    getPoliticaPrivacidad: function (req, res, next) {
        var util = utilFncs.init(req, res);
        util.render('politica-privacidad', {
            noamp: true
        });
    },
    getPoliticaCookies: function (req, res, next) {
        var util = utilFncs.init(req, res);
        util.render('politica-cookies', {
            noamp: true
        });
    },

}

module.exports = Directorio;


