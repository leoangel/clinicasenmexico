var errorLog = require('../library/util/errorlog');
var config = require('../config/contact');
var request = require('request');
var nodemailer = require('nodemailer');
var sanitizer = require('sanitizer');
var utilFncs = require('../library/util/UtilFncs');

var ContactController = {
    getForm: function (req, res) {

        var util = utilFncs.init(req, res);

        util.render('contacto', {
            invalidCaptcha: req.query.invalidCaptcha ? true : false,
            noamp: true
        });
    },
    /**
     * Envia mensaje desde formulario de contacto
     */
    sendMessage: function (req, res) {
        var view = 'contacto/contacto_enviado';
        if (mobile) {
            view = 'contacto/contacto_enviado_mobile';
        }
        this.googleVerify(req, function () {
            ContactController.sendEmail(req.body, function (error) {
                res.render(view, {
                    isMobile: mobile,
                    error: error
                });
            });
        }, function () {
            var url = mobile ? '/m/contacto' : 'contacto';
            res.redirect(url + '?invalidCaptcha=true');
        });
    },
    /**
     * Verifica el captcha
     */
    googleVerify: function (req, fncSucces, fncErr) {

        var secretKey = config.captcha_secret;
        var verificationUrl = config.captcha_url + "?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
        // Hitting GET request to the URL, Google will respond with success or error scenario.
        request(verificationUrl, function (error, response, body) {
            if (error) {
                errorLog.error('Error al realizar la verificacion del captcha en Google');
                fncErr();
            } else {
                body = JSON.parse(body);
                // Success will be true or false depending upon captcha validation.
                if (body.success !== undefined && !body.success) {
                    fncErr();
                }
                fncSucces();
            }
        });
    },
    /**
     * Envia el email
     */
    sendEmail: function (data, callback) {

        var asunto = sanitizer.escape(data.asunto); 
        var email = sanitizer.escape(data.email); 
        var accion = sanitizer.escape(data.accion);
        var url = sanitizer.escape(data.url);
        var backUrl = sanitizer.escape(data.back);
        var msg = sanitizer.escape(data.msg);

        var mailOptions = {
            from: config.email,
            to: config.email,
            subject: config.site_name,
            text: accion + '\n' + asunto + '\n\n' + email + '\n\n' + 'Url: ' + url + '\n\n' + 'BackUrl: ' + backUrl + '\n' + msg
        };

        var transporter = nodemailer.createTransport(config.smtpConfig);
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                errorLog.error('Error al enviar el correo');
                errorLog.error(error);
                callback(true);
            } else
                callback(false);
        });
    }
}

module.exports = ContactController;


