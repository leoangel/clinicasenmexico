var dao = require('../dao/SearchDAO');
var utilFncs = require('../library/util/UtilFncs');

var CustomQueries = {

    /**
     * Busquedas personalizadas creadas a partir de busquedas de usuarios o investigacion
     */
    customQueries: function (req, res) {

        let query = req.params.consulta;
        var util = utilFncs.init(req, res);

        dao.customQueriesByUrl(query).then(function (consulta, results) {

            let data = { consulta: consulta };
            let view = consulta.static;

            if (consulta.tipo == 'query') {
                data.results = results;
                view = (consulta.static == null)? 'consulta' : consulta.static;
            }

            util.render(view, data, true, 'custom');

        }, function (err, origen) {
            util.errorSend404(err, origen)
        });
    },

    /**
     * Consultas populares encontradas en webmater y diferentes herramientas
     */
    consultasPopulares: (req, res) => {

        var util = utilFncs.init(req, res);
        var dao = require('../dao/DirectorioDAO');

        dao.findPopulares().then(function (results) {
            util.render('consultas', {
                data: results,
            });
        }, function (err, origen) {
            util.errorSend404(err, origen)
        });
    },

    jsonConsultas: (req, res) => {

        var util = utilFncs.init(req, res);
        var dao = require('../dao/DirectorioDAO');

        dao.findPopulares().then(function (results) {
            util.json(results);
        }, function (err, origen) {
            util.jsonError(err, origen)
        });
    },

    /**
     * Genera paginas html y las almacena en /public/customqueries/
     */
    generateStaticView: function (req, res) {

        /**
         * 1. Consultar todas las consutlas
         * 2. iterar y las que esten habilitadas para archico estatico guardar
         * 3. iterar y realizar peticiones a las urls guardadas y obtener el codigo html
         * 4. comprimir el codigo html y guadar en el disco
         */

        var util = utilFncs.init(req, res);
        dao.AllStaticCustomQueries().then(function (results) {
            util.res.send('Generando ' + results.length + ' vistas');

            var request = require('request').defaults({ rejectUnauthorized: false });
            var url = util.getBaseUrl() + '/' + util.getCustomQueriesPath();

            results.forEach(function (r) {

                request(url + '/' + r.url, function (error, response, html) {
                    if (!error && response.statusCode == 200) {

                        var fs = require('fs');

                        var path = __dirname + '/../public/' + util.getCustomQueriesPath();

                        // Crea el path si no existe
                        if (!fs.existsSync(path)) {
                            fs.mkdirSync(path);
                        }


                        // Verifica si el archivo existe    
                        var pathFile = path + '/' + r.url;
                        if (fs.existsSync(pathFile))
                            return;

                        // Guarda el archivo con el nombre o url de la consulta sin extension
                        fs.writeFileSync(pathFile, html, {
                            encoding: 'utf8'
                        });

                        // Crea la version .gz
                        var zlib = require('zlib');
                        const gzip = zlib.createGzip();
                        const inp = fs.createReadStream(pathFile);
                        const out = fs.createWriteStream(pathFile + '.gz');
                        inp.pipe(gzip).pipe(out);

                    } else {
                        console.log("Error " + response.statusCode);
                        console.log(error);
                    }
                });

            });

        }, function (err, origen) {
            util.errorSend404(err, origen)
        });
    }

}

module.exports = CustomQueries;


