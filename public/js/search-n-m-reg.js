var temp_auto_reg = '<div id="reg-autoc-loc" class="autoc-loc" style="display:none;"><div class="large-12 columns"><span><i class="dashicons dashicons-location"></i> Donde ?</span> <ul id="auto-list-loc-reg" class="auto-list-loc"></ul></div></div>';

var parser = new DOMParser();
var doc_reg = parser.parseFromString(temp_auto_reg, "text/html");

var autoc_reg = document.getElementById('regiones');
var locsel_reg = document.getElementById('regiones-locsel');
var auto_l_reg = doc_reg.getElementById('reg-autoc-loc');
var listlo_reg = doc_reg.getElementById('auto-list-loc-reg');

autoc_reg.appendChild(auto_l_reg);
locsel_reg.addEventListener("input", evLocselInput);
locsel_reg.addEventListener("focus", function (e) {
    if (this.value.length > 0) {
        if (listlo_reg.innerHTML.length == 0) {
            evLocselInput(e);
        } else {
            showAutoCl();
        }
    }
});

function evLocselInput(e) {

    let inp = this;
    let val = this.value;

    if (val && val.length == 0)
        return hideAutoCl();

    // Consulta servicios
    query(val, 3, function (data) {
        listlo_reg.innerHTML = '';
        if (data.length > 0)
            data.forEach(function (l) {
                listlo_reg.appendChild(getListItem(l, inp));
            });
        else {
            listlo_reg.appendChild(getListInfo());
            query(val, 30, function (data) {
                data.forEach(function (l) {
                    listlo_reg.appendChild(getListItem(l, inp));
                });
            });
        }
    });

    showAutoCl();
}

document.addEventListener("click", function (e) {
    if (e.target != locsel_reg)
        hideAutoCl();
});

function showAutoCl() {
    auto_l_reg.style.display = 'block';
}
function hideAutoCl() {
    auto_l_reg.style.display = 'none';
}

document.getElementById('regiones-busact').addEventListener("click", function(e){
    if(lasItemClick)
        window.location = lasItemClick.dataset.href;
    else    
        window.location = '/buscar?q=' + encodeURIComponent(locsel_reg.value).replace(/%20/g, "+");
});