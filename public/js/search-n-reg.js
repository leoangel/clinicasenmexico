var temp_auto_loc = '<div id="autoc-loc-regiones"><div class="large-12"><span><i class="dashicons dashicons-location"></i> Donde ?</span> <ul id="auto-list-loc-regiones"></ul></div></div>';

parser = new DOMParser();
docreg = parser.parseFromString(temp_auto_loc, "text/html");

auto_c_reg = document.getElementById('regiones');
auto_l_reg = docreg.getElementById('autoc-loc-regiones');

const locselreg = document.getElementById('locsel-regiones');
const listloreg = docreg.getElementById('auto-list-loc-regiones');

var urlU = '/#url';

auto_c_reg.appendChild(auto_l_reg);

locselreg.addEventListener("input", function () {

    let inp = this;
    let val = this.value;
    
    if(val.length == 0){
        return hideAutoClReg();
    }

    // Consulta servicios
    query(val, 3, function(data){
        listloreg.innerHTML = '';
        if(data.length > 0)
            data.forEach(function(l){
               listloreg.appendChild(getListItem(l, inp)); 
            });
        else {
            listloreg.appendChild(getListInfo());
            query(val, 30, function(data){
                data.forEach(function(l){
                   listloreg.appendChild(getListItem(l, inp)); 
                });                
            });
        }    
    });	
    showAutoClReg();
});

locselreg.addEventListener("focus", function (e) {
    e.stopPropagation();
    if(this.value.length > 0){
        if(listloreg.innerHTML.length == 0){
            inputTri(locselreg);
        } else {
            showAutoClReg();
        }
    }    
});

function showAutoClReg(){
    auto_l_reg.style.display = 'flex';
}
function hideAutoClReg(){
    auto_l_reg.style.display = 'none';
}

document.addEventListener("click", function (e) {
    if(e.target != locselreg){
        hideAutoClReg();    
    }        
});

var itemAc = null;


document.getElementById('busact-regiones').addEventListener('click', function(e){
    
    if(locselreg.value == '')
        return;
    
    if(itemAc != null && itemAc.dataset.item == locselreg.value){
        window.location = itemAc.dataset.href; 
    } else
        window.location = '/buscar?q=' + encodeURIComponent(locselreg.value).replace(/%20/g, "+");
});
