var temp_auto = '<div id="autoc-serv" style="display:none"><div class="large-6 columns"> <span><i class="dashicons dashicons-admin-generic"></i> Servicios</span> <ul id="auto-list-serv"></ul> </div><div class="large-6 columns"> <span><i class="dashicons dashicons-admin-home"></i> Centros</span> <ul id="auto-list-estb"></ul> </div></div><div id="autoc-loc" class="autoc-loc" style="display:none"><div class="large-12"><span><i class="dashicons dashicons-location"></i> Donde ?</span> <ul id="auto-list-loc"></ul></div></div>';

var parser = new DOMParser();
var doc = parser.parseFromString(temp_auto, "text/html");

auto_c = document.getElementById('autoc-bus');
auto_s = doc.getElementById('autoc-serv');

const servse = document.getElementById('servsel');
const listse = doc.getElementById('auto-list-serv');
const listes = doc.getElementById('auto-list-estb');
const listlo = doc.getElementById('auto-list-loc');
const busact = document.getElementById('busact');

var urlS = '/especialidad/#url';
var urlE = '/ips/#url';
var urlU = '/#url';
var urlQ = '/directorio/#url';

auto_c.appendChild(auto_s);

servse.addEventListener("input", function () {

    let inp = this;
    let val = this.value;

    if (val.length == 0)
        return hideAutoC();

    // Consulta servicios
    query(val, 1, function (data) {
        listse.innerHTML = '';
        if (data.length > 0)
            appendItemList(data, listse, inp);
        else {
            listse.appendChild(getListInfo());
            query(val, 10, function (data) {
                appendItemList(data, listse, inp);
            });
        }
    });

    // Consulta establecimientos
    query(val, 2, function (data) {
        listes.innerHTML = '';
        if (data.length > 0)
            appendItemList(data, listes, inp);
        else {
            listes.appendChild(getListInfo('No hay coincidencias "' + val + '"'));
        }
    });
    // Si no es vacio enviar consulta
    showAutoC();
});
servse.addEventListener("focus", function (e) {
    if (this.value.length > 0) {
        if (listse.innerHTML.length == 0) {
            inputTri(e.target);
        } else {
            showAutoC();
        }
    }
});
servse.addEventListener("keydown", function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        busact.click();
    }
});
document.addEventListener("click", function (e) {
    if (e.target != servse)
        hideAutoC();
});


function showAutoC() {
    auto_s.style.display = 'block';
}
function hideAutoC() {
    auto_s.style.display = 'none';
}


function getListItem(data, input) {

    value = input.value;
    label = data.nombre;
    li = document.createElement('li');
    li.dataset.item = label;

    let href = null;
    if (data.t == 's') { href = urlS.replace('#url', data.url) }
    if (data.t == 'e') { href = urlE.replace('#url', data.url) }
    if (data.t == 'u') { href = urlU.replace('#url', data.url) }
    if (data.t == 'q') { href = urlQ.replace('#url', data.url) }

    li.dataset.href = href;
    href = href + '?a=1';

    re = new RegExp(value, "i");
    label = label.replace(re, function (x) { return '<strong data-item="'+label+'">' + x + '</strong>' });

    li.innerHTML = label;
    li.addEventListener("click", function (e) {
        input.value = e.target.dataset.item;
        guardarSel(e.target);
    });
    return li;
}

function getListInfo(label) {
    let li = document.createElement('li');
    li.setAttribute('class', 'info-sug');
    li.innerHTML = (label) ? label : 'Sugerencias:';
    return li;
}

function appendItemList(data, list, inp){
    data.forEach(function (l) {
        list.appendChild(getListItem(l, inp));
    });
}



function query(v = '', t = '', c) {
    /**
     * Si la consulta son servicios y el almacenamiento local esta activo
     * hace la consulta de todos los servicios y almacena en local y devuelve 
     * la consulta
     */
    v = unaccent(v.toLowerCase());
    if ((t == 1 || t == 10) && hlSt) {
        if (lStServices == null) {
            xhrC('/json/autoc/search?t=11&' + 'v=' + v, function (data) {
                localStorage.setItem('__services_autoc_n', JSON.stringify(data));
                lStServices = data;

                if (t == 1)
                    queryLocal(v, c);
                else
                    suggLocal(v, c);
            });
        } else {
            if (t == 1)
                queryLocal(v, c);
            else
                suggLocal(v, c);
        }
    } else
        xhrC('/json/autoc/search?' + 'v=' + v + '&t=' + t, function (data) {
            c(data);
        });
}

function queryLocal(v, c) {

    let services = [];
    v = unaccent(v.toLowerCase());

    if (v.length == 1) {
        services = lStServices.filter(function (a) {
            nombre = unaccent(a.nombre.toLowerCase());
            return nombre.substring(0, 1) == v
        });
    } else {
        let serLev = [];
        lStServices.filter(function (a) {
            nombre = unaccent(a.nombre.toLowerCase());
            if (nombre.indexOf(v) !== -1) {
                a.lev = levenshtein(a.nombre, v);
                serLev.push(a);
                return true;
            }
        });
        services = serLev.sort((a, b) => a.lev - b.lev);
    }
    c(services.slice(0, 6));
}

function suggLocal(v, c) {

    let serLev = [];

    lStServices.forEach(function (a) {
        a.lev = levenshtein(a.nombre, v);
        serLev.push(a);
    });
    services = serLev.sort((a, b) => a.lev - b.lev);
    c(services.slice(0, 6));
}



// window.addEventListener("orientationchange", function () {
//     setTimeout(function () { setAutocLocPos(); }, 100);
// });

function inputTri(e) {
    var event = new Event('input', { 'bubbles': true, 'cancelable': true });
    e.dispatchEvent(event);
}

busact.addEventListener('click', function (e) {

    var ser = servse.value;

    if (ser.length == 0)
        return;

    // Si ambos son urls
    if (itemAcS && itemAcS.dataset.item == ser) {
        url = itemAcS.dataset.href;
        return window.location = url + '?a=9';
    }

    return window.location = '/buscar?q=' + encodeURIComponent(ser).replace(/%20/g, "+");
});

var itemAcS = null;
var lasItemClick = null;
function guardarSel(t) {
    if (t.parentElement.id == 'auto-list-serv')
        itemAcS = t;

    lasItemClick = t;
}


/********************************************************************************************** */
/** functions */

function levenshtein(e, t) {
    if (0 === e.length) return t.length;
    if (0 === t.length) return e.length;
    var r, n, l = [];
    for (r = 0; r <= t.length; r++) l[r] = [r];
    for (n = 0; n <= e.length; n++) l[0][n] = n;
    for (r = 1; r <= t.length; r++)
        for (n = 1; n <= e.length; n++) t.charAt(r - 1) == e.charAt(n - 1) ? l[r][n] = l[r - 1][n - 1] : l[r][n] = Math.min(l[r - 1][n - 1] + 1, Math.min(l[r][n - 1] + 1, l[r - 1][n] + 1));
    return l[t.length][e.length]
}

var unaccent = function (s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp("[àáâãäå]", 'g'), "a");
    r = r.replace(new RegExp("æ", 'g'), "ae");
    r = r.replace(new RegExp("ç", 'g'), "c");
    r = r.replace(new RegExp("[èéêë]", 'g'), "e");
    r = r.replace(new RegExp("[ìíîï]", 'g'), "i");
    r = r.replace(new RegExp("ñ", 'g'), "n");
    r = r.replace(new RegExp("[òóôõö]", 'g'), "o");
    r = r.replace(new RegExp("œ", 'g'), "oe");
    r = r.replace(new RegExp("[ùúûü]", 'g'), "u");
    r = r.replace(new RegExp("[ýÿ]", 'g'), "y");
    return r;
};


function remToPx(rem) {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

// xhr
function xhrC(url, fnc, err) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 0 && fnc)
                fnc(JSON.parse(this.responseText))
        }
    };
    xhr.onerror = function () {
        err();
    };
    xhr.open("GET", url, true);
    xhr.send()
}

// Si hay almacenamiento local
var hlSt = false;
var lStServices = null;
if (typeof (Storage) !== "undefined") {
    hlSt = true;
    if (typeof localStorage.getItem('__services_autoc_n') == 'string')
        lStServices = JSON.parse(localStorage.getItem('__services_autoc_n'));
}

// Consultas populares
var getConsultas = function () {
    return new Promise(function (resolve, reject) {

        if (typeof (Storage) !== "undefined") {
            if (typeof localStorage.getItem('__dirnx_consultas') == 'string')
                return resolve(JSON.parse(localStorage.getItem('__dirnx_consultas')));
        }

        xhrC('/json/consultas', function (data) {
            localStorage.setItem('__dirnx_consultas', JSON.stringify(data));
            resolve(data);
        }, function (err) {
            reject(err);
        });

    });
}
var consultasBtn = document.getElementById("consultas");
if (consultasBtn)
    consultasBtn.onclick = function (e) {
        return;
        e.preventDefault();

        getConsultas().then(function (consultas) {
            console.log(consultas)
        }).catch(function (err) {
            console.log(err)
        });
    }

if (typeof (Storage) !== "undefined") {
    xhrC('/json/update-version', function (data) {
        if (data.version && localStorage.getItem('__dirnx_version') != data.version) {
            console.log("Update version " + data.version);
            localStorage.setItem('__dirnx_version', data.version);
            localStorage.removeItem('__dirnx_consultas');
            localStorage.removeItem('__services_autoc_n');
        }
    });
}

/*************************************************************************************************
 * Si hay archivos js para cargar despues
 */
if (typeof loadSCB === "function") {

    var addJSCB = function (url) {
        var script = document.createElement("script");
        script.src = url;
        document.head.appendChild(script);
    }
    loadSCB();
}