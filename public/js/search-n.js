var temp_auto = '<div id="autoc-serv" style="display:none;"><div class="large-6 columns br-auto"> <span><i class="dashicons dashicons-admin-generic"></i> Servicios</span> <ul id="auto-list-serv"></ul> </div><div class="large-6 columns"> <span><i class="dashicons dashicons-admin-home"></i> Centros</span> <ul id="auto-list-estb"></ul> </div></div><div id="autoc-loc" style="display:none"><div class="large-12"><span><i class="dashicons dashicons-location"></i> Donde ?</span> <ul id="auto-list-loc"></ul></div></div><div id="autoc-consultas" class="autoc-cnt"></div>';

var parser = new DOMParser();
var doc = parser.parseFromString(temp_auto, "text/html");

var auto_c = document.getElementById('autoc-bus');
var auto_s = doc.getElementById('autoc-serv');
var auto_l = doc.getElementById('autoc-loc');

const servse = document.getElementById('servsel');
const locsel = document.getElementById('locsel');
const busact = document.getElementById('busact');
const listse = doc.getElementById('auto-list-serv');
const listes = doc.getElementById('auto-list-estb');
const listlo = doc.getElementById('auto-list-loc');

var urlS = '/especialidad/#url';
var urlE = '/ips/#url';
var urlU = '/#url';
var urlQ = '/directorio/#url';

auto_c.appendChild(auto_s);
auto_c.appendChild(auto_l);

servse.addEventListener("input", function () {

    var inp = this;
    var val = this.value;

    if (val.length == 0)
        return hideAutoC();

    // Consulta servicios
    query(val, 1, function (data) {
        listse.innerHTML = '';
        if (data.length > 0)
            data.forEach(function (l) {
                listse.appendChild(getListItem(l, inp));
            });
        else {
            listse.appendChild(getListInfo());
            query(val, 10, function (data) {
                data.forEach(function (l) {
                    listse.appendChild(getListItem(l, inp));
                });
            });
        }
    });

    // Consulta establecimientos
    query(val, 2, function (data) {
        listes.innerHTML = '';
        if (data.length > 0)
            data.forEach(function (l) {
                listes.appendChild(getListItem(l, inp));
            });
        else {
            listes.appendChild(getListInfo('No hay coincidencias "' + val + '"'));
        }
    });
    // Si no es vacio enviar consulta
    showAutoC();
});

locsel.addEventListener("input", function () {

    var inp = this;
    var val = this.value;

    if (val.length == 0)
        return hideAutoCl();

    // Consulta servicios
    query(val, 3, function (data) {
        listlo.innerHTML = '';
        if (data.length > 0)
            data.forEach(function (l) {
                listlo.appendChild(getListItem(l, inp));
            });
        else {
            listlo.appendChild(getListInfo());
            query(val, 30, function (data) {
                data.forEach(function (l) {
                    listlo.appendChild(getListItem(l, inp));
                });
            });
        }
    });

    showAutoCl();
});

servse.addEventListener("focus", function (e) {
    hideAutoCl();
    if (this.value.length > 0) {
        if (listse.innerHTML.length == 0) {
            inputTri(e.target);
        } else {
            showAutoC();
        }
    }
});

locsel.addEventListener("focus", function (e) {
    hideAutoC();
    if (this.value.length > 0) {
        if (listse.innerHTML.length == 0) {
            inputTri(e.target);
        } else {
            showAutoCl();
        }
    }
});

function showAutoC() {
    auto_s.style.display = 'flex';
}
function hideAutoC() {
    auto_s.style.display = 'none';
}
function showAutoCl() {
    auto_l.style.display = 'flex';
}
function hideAutoCl() {
    auto_l.style.display = 'none';
}

servse.addEventListener("keydown", function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        busact.click();
    }
});

locsel.addEventListener("keydown", function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        busact.click();
    }
});

var closeFncs = [];
document.addEventListener("click", function (e) {
    closeFncs.forEach(function (f) {
        f(e.target);
    });
});

closeFncs.push(function (target) {
    if (target != servse)
        hideAutoC();
    if (target != locsel)
        hideAutoCl();
});

function getListItem(data, input) {

    value = input.value;
    label = data.nombre;
    li = document.createElement('li');
    li.dataset.item = label;

    var href = null;
    if (data.t == 's') { href = urlS.replace('#url', data.url) }
    if (data.t == 'e') { href = urlE.replace('#url', data.url) }
    if (data.t == 'u') { href = urlU.replace('#url', data.url) }
    if (data.t == 'q') { href = urlQ.replace('#url', data.url) }

    li.dataset.href = href;
    href = href + '?a=1';

    re = new RegExp(value, "i");
    label = label.replace(re, function (x) { return '<strong>' + x + '</strong>' });

    li.innerHTML = label + '<a href="' + href + '" title="Ver enlace"><i class="dashicons dashicons-external"></i></a>';
    li.addEventListener("click", function (e) {
        if(e.target.tagName == 'I') return;
        var elem = (e.target.tagName == 'STRONG') ? e.target.parentElement : e.target;
        input.value = elem.dataset.item;
        guardarSel(e.target);
    });
    return li;
}

function getListInfo(label) {
    var li = document.createElement('li');
    li.setAttribute('class', 'info-sug');
    li.innerHTML = (label) ? label : 'Sugerencias:';
    return li;
}

// xhr
function xhrC(url, fnc) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText.length > 0 && fnc)
                fnc(JSON.parse(this.responseText))
        } else {
            // @TODO registrar error
        }
    };
    xhr.open("GET", url, true);
    xhr.send()
}

var hlSt = false;
var lStServices = null;
// Si hay almacenamiento local
if (typeof (Storage) !== "undefined") {
    hlSt = true;
    if (typeof localStorage.__services_autoc_n == 'string')
        lStServices = JSON.parse(localStorage.__services_autoc_n);

    checkVersion();
}
function query(v = '', t = '', c) {
    /**
     * Si la consulta son servicios y el almacenamiento local esta activo
     * hace la consulta de todos los servicios y almacena en local y devuelve 
     * la consulta
     */
    v = unaccent(v.toLowerCase());
    if ((t == 1 || t == 10) && hlSt) {
        if (lStServices == null) {
            xhrC('/json/autoc/search?t=11&' + 'v=' + v, function (data) {
                localStorage.__services_autoc_n = JSON.stringify(data);
                lStServices = data;

                if (t == 1)
                    queryLocal(v, c);
                else
                    suggLocal(v, c);
            });
        } else {
            if (t == 1)
                queryLocal(v, c);
            else
                suggLocal(v, c);
        }
    } else
        xhrC('/json/autoc/search?' + 'v=' + v + '&t=' + t, function (data) {
            c(data);
        });
}

function queryLocal(v, c) {

    var services = [];
    v = unaccent(v.toLowerCase());

    if (v.length == 1) {
        services = lStServices.filter(function (a) {
            nombre = unaccent(a.nombre.toLowerCase());
            return nombre.substring(0, 1) == v
        });
    } else {
        var serLev = [];
        lStServices.filter(function (a) {
            nombre = unaccent(a.nombre.toLowerCase());
            if (nombre.indexOf(v) !== -1) {
                a.lev = levenshtein(a.nombre, v);
                serLev.push(a);
                return true;
            }
        });
        services = serLev.sort((a, b) => a.lev - b.lev);
    }
    c(services.slice(0, 6));
}

function suggLocal(v, c) {

    var serLev = [];

    lStServices.forEach(function (a) {
        a.lev = levenshtein(a.nombre, v);
        serLev.push(a);
    });
    services = serLev.sort((a, b) => a.lev - b.lev);
    c(services.slice(0, 6));
}

function levenshtein(e, t) {
    if (0 === e.length) return t.length;
    if (0 === t.length) return e.length;
    var r, n, l = [];
    for (r = 0; r <= t.length; r++) l[r] = [r];
    for (n = 0; n <= e.length; n++) l[0][n] = n;
    for (r = 1; r <= t.length; r++)
        for (n = 1; n <= e.length; n++) t.charAt(r - 1) == e.charAt(n - 1) ? l[r][n] = l[r - 1][n - 1] : l[r][n] = Math.min(l[r - 1][n - 1] + 1, Math.min(l[r][n - 1] + 1, l[r - 1][n] + 1));
    return l[t.length][e.length]
}

var unaccent = function (s) {
    var r = s.toLowerCase();
    r = r.replace(new RegExp("[àáâãäå]", 'g'), "a");
    r = r.replace(new RegExp("æ", 'g'), "ae");
    r = r.replace(new RegExp("ç", 'g'), "c");
    r = r.replace(new RegExp("[èéêë]", 'g'), "e");
    r = r.replace(new RegExp("[ìíîï]", 'g'), "i");
    r = r.replace(new RegExp("ñ", 'g'), "n");
    r = r.replace(new RegExp("[òóôõö]", 'g'), "o");
    r = r.replace(new RegExp("œ", 'g'), "oe");
    r = r.replace(new RegExp("[ùúûü]", 'g'), "u");
    r = r.replace(new RegExp("[ýÿ]", 'g'), "y");
    return r;
};


function remToPx(rem) {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

window.addEventListener("orientationchange", function () {
    setTimeout(function () { setAutocLocPos(); }, 100);
});

function setAutocLocPos() {

    var padd = (window.innerWidth < remToPx(64)) ? remToPx(0.65) : remToPx(2);
    // pos autoc-loc
    var a = getComputedStyle(auto_l);
    if (a.position == 'absolute') {
        auto_l.style.left = locsel.offsetLeft + 'px';
        auto_l.style.width = locsel.offsetWidth + 'px';
    } else
        auto_l.style.left = (locsel.offsetLeft - auto_c.offsetLeft - padd) + 'px';
}
setAutocLocPos();

function inputTri(e) {
    var event = new Event('input', { 'bubbles': true, 'cancelable': true });
    e.dispatchEvent(event);
}

busact.addEventListener('click', function (e) {

    var loc = locsel.value;
    var ser = servse.value;

    if (loc.length == 0 && ser.length == 0)
        return;

    // Si ambos son urls
    if (itemAcL && itemAcS && itemAcL.dataset.item == loc && itemAcS.dataset.item == ser) {
        urlS = itemAcS.dataset.href.replace('/especialidad', '');
        urlL = itemAcL.dataset.href;
        return window.location = urlL + urlS + '?a=2';
    }

    q = ser + ' en ' + loc;
    if (ser.length == 0) q = loc;
    if (loc.length == 0) q = ser;

    return window.location = '/buscar?q=' + encodeURIComponent(q).replace(/%20/g, "+");
});

var itemAcS = null;
var itemAcL = null;
function guardarSel(t) {
    if (t.parentElement.id == 'auto-list-serv')
        itemAcS = t;
    if (t.parentElement.id == 'auto-list-loc')
        itemAcL = t;
}

// Consultas
var autoc_consultas = doc.getElementById("autoc-consultas");
var consultasBtn = document.getElementById("consultas");
var flgConsultas = false;

auto_c.appendChild(autoc_consultas);

if (consultasBtn) {
    function eConsultas(e) {
        e.preventDefault();
          
        showAutoCc();
        if (flgConsultas)
            return;
        
        getConsultas().then(function (consultas) {  
            consultasSetList(consultas);
            flgConsultas = true;
        }).catch(function (err) {
            console.log(err)
        });
    }
    consultasBtn.addEventListener('click', eConsultas);
}
function consultasSetList(data) {
    var list = document.createElement('ul');
    data.forEach(function (l) {
        list.appendChild(getListLink(l));
    });
    var vm = getListLink({url:'/consultas', title: 'Ver más'});
    vm.className = 'vm';
    list.appendChild(vm);
    var title = document.createElement('span');
    title.innerHTML = '<i class="dashicons dashicons-search"></i></a> Consultas populares';
    autoc_consultas.appendChild(title);
    autoc_consultas.appendChild(list);
}
function getListLink(data) {
    li = document.createElement('li');
    li.innerHTML = '<a href="' + data.url + '" title="' + data.title + '">' + data.title + '<i class="dashicons dashicons-external"></i></a>';
    return li;
}

// Consultas populares
var getConsultas = function () {
    return new Promise(function (resolve, reject) {

        if (typeof (Storage) !== "undefined") {
            if (typeof localStorage.getItem('__dirnx_consultas') == 'string')
                return resolve(JSON.parse(localStorage.getItem('__dirnx_consultas')));
        }

        xhrC('/json/consultas', function (data) {
            localStorage.setItem('__dirnx_consultas', JSON.stringify(data));
            resolve(data);
        }, function (err) {
            reject(err);
        });

    });
}
function showAutoCc() {   
    autoc_consultas.style.display = 'block';
}
function hideAutoCc() {
    autoc_consultas.style.display = 'none';
}
closeFncs.push(function (target) {
    if (consultasBtn && target != consultasBtn && target != consultasBtn.querySelector('i'))
        hideAutoCc();
});

function checkVersion() {

    var dt = new Date();
    var ltime = dt.getTime();
    // 1 para validar nuevamente
    if (localStorage.getItem('__dirnx_version_time') && localStorage.getItem('__dirnx_version_time') > ltime - 3600000)
        return;

    xhrC('/json/update-version', function (data) {
        if (data.version && localStorage.getItem('__dirnx_version') != data.version) {
            console.log("Update version " + data.version);
            localStorage.setItem('__dirnx_version', data.version);
            localStorage.removeItem('__dirnx_consultas');
            localStorage.removeItem('__services_autoc_n');
        }
        localStorage.setItem('__dirnx_version_time', ltime);
    });
}