(function () {
    'use strict';
    angular
            .module('autocomplete', ['ngMaterial'])
            .controller('DemoCtrl', DemoCtrl);
    function DemoCtrl($timeout, $q, $log, $http) {
        var self = this;
        self.simulateQuery = false;
        self.isDisabled = false;
        self.fshowLinks = false;
        // list of `state` value/display objects
        self.serv = loadAll();
        self.countServ = 0;
        self.querySearch = querySearch;
        self.selectedItemChange = selectedItemChange;
        self.searchTextChange = searchTextChange;
        self.resultsQuery = [];
        self.display = display;
        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {
            self.query = query;
            return query ? filterMunicipios(query) : self.serv;
        }
        function searchTextChange(text) {
            return;
            //$log.info('Text changed to ' + text);
        }
        function selectedItemChange(item) {
            return;
            //$log.info('Item changed to ' + JSON.stringify(item));
        }
        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            return $http.get('/json/servicios').then(function (response) {
                self.countServ = response.data.count;
                return response.data.servicios;
            });
        }
        /**
         * Create filter function for a query string
         */
        function filterMunicipios(query) {
            var lowercaseQuery = angular.lowercase(query);
            self.resultsQuery = [];

            self.serv['$$state'].value.forEach(function (item) {
                var text = item.servicio;
                text.toLowerCase().indexOf(lowercaseQuery)
                if (text.toLowerCase().indexOf(lowercaseQuery) !== -1)
                    self.resultsQuery.push(item);
            });

            return self.resultsQuery;
        }

        function display(item) {
            return item.servicio;
        }
    }
})();