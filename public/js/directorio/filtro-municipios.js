(function () {
  'use strict';
  angular
      .module('autocomplete', ['ngMaterial'])
      .controller('DemoCtrl', DemoCtrl);
  function DemoCtrl ($timeout, $q, $log, $http) {
    var self = this;
    self.simulateQuery = false;
    self.isDisabled    = false;
    self.fshowLinks     = false; 
    self.showLinks     = showLinks;
    // list of `state` value/display objects
    self.mun           = loadAll();
    self.countMun      = 0; 
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    self.resultsQuery  = [];
    self.display       = display;
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
        self.query = query;
        return query ? filterMunicipios(query) : self.mun;
    }
    function searchTextChange(text) {
      return;  
      //$log.info('Text changed to ' + text);
    }
    function selectedItemChange(item) {
      return;  
      //$log.info('Item changed to ' + JSON.stringify(item));
    }
    /**
     * Build `states` list of key/value pairs
     */
    function loadAll() {
       return $http.get('/json/'+serv_url+'/municipios').then(function(response){
            self.countMun = response.data.count;
            return response.data.municipios;
       });
    }
    /**
     * Create filter function for a query string
     */
    function filterMunicipios(query) {
      var lowercaseQuery = angular.lowercase(query);
      self.resultsQuery = [];

      self.mun['$$state'].value.forEach(function(item){
        var text = item.municipio + ' ' + item.entidad;
        text.toLowerCase().indexOf(lowercaseQuery)
        if(text.toLowerCase().indexOf(lowercaseQuery) !== -1)
            self.resultsQuery.push(item);
      });

      return self.resultsQuery;
    }

    function showLinks(){

        var linksContainer = angular.element(document.querySelector('#municipios-links'));
        linksContainer.empty();

        var baseU = (mob) ? '/m' : '';    

        if(self.fshowLinks){
            self.mun['$$state'].value.forEach(function(item){
              linksContainer.append('<div class="municipio"><a href="'+baseU+'/'+serv_url+'/'+item.region_url+'/'+item.subregion_url+'">'+item.municipio+', '+item.entidad+'</a><span>'+item.count+' Resultados</span></div>'); 
            });
        }
    }

    function display(item){
        return item.municipio + ', ' + item.entidad;
    }
  }
})();