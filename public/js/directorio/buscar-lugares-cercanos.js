var x = document.getElementById("geoerror");
var info = document.getElementById("geoinfouser");

if (window.location.hash == '#habilitar-geolocalizacion') {
    document.getElementById("habilitar-geolocalizacion").style.display = "block";
}

function showError(error) {
    info.innerHTML = "";
    switch (error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "Has denegado la Geolocalización.";
            document.getElementById("habilitar-geolocalizacion").style.display = "block";
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "La Geolocalización no esta disponible."
            break;
        case error.TIMEOUT:
            x.innerHTML = "La petición ha exedido el tiempo."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "Se ha generado un error al obtener la ubicación."
            break;
    }
}


(function () {
    'use strict';
    angular
            .module('autocomplete', ['ngMaterial'])
            .controller('DemoCtrl', DemoCtrl);
    function DemoCtrl($timeout, $q, $log, $http) {
        var self = this;
        self.simulateQuery = false;
        self.isDisabled = false;
        self.fshowLinks = false;
        // list of `state` value/display objects
        self.serv = loadAll();
        self.countServ = 0;
        self.querySearch = querySearch;
        self.selectedItemChange = selectedItemChange;
        self.searchTextChange = searchTextChange;
        self.resultsQuery = [];
        self.display = display;
        self.encontrarLugares = encontrarLugares;

        // ******************************
        // Internal methods
        // ******************************
        /**
         * Search for states... use $timeout to simulate
         * remote dataservice call.
         */
        function querySearch(query) {
            console.log(query);
            self.query = query;
            return query ? filterServicios(query) : self.serv;
        }
        function searchTextChange(text) {
            return;
            //$log.info('Text changed to ' + text);
        }
        function selectedItemChange(item) {
            return;
            //$log.info('Item changed to ' + JSON.stringify(item));
        }
        /**
         * Build `states` list of key/value pairs
         */
        function loadAll() {
            return $http.get('/json/servicios').then(function (response) {
                self.countServ = response.data.count;
                return response.data.servicios;
            });
        }
        /**
         * Create filter function for a query string
         */
        function filterServicios(query) {
            var lowercaseQuery = angular.lowercase(query);
            self.resultsQuery = [];

            self.serv['$$state'].value.forEach(function (item) {
                var text = item.servicio;
                text.toLowerCase().indexOf(lowercaseQuery)
                if (text.toLowerCase().indexOf(lowercaseQuery) !== -1)
                    self.resultsQuery.push(item);
            });

            return self.resultsQuery;
        }

        function display(item) {
            return item.servicio;
        }

        function encontrarLugares() {
            info.innerHTML = "Obteniendo ubicación...";
            getLocation();
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        ;

        function showPosition(position) {

            var q, actividad_scian = '';
            var inval = document.getElementsByName("busqueda_cercanos")[0].value;

            if (self.selectedItem) {
                q = self.selectedItem.servicio;
                actividad_scian = encodeURIComponent(self.selectedItem.actividad_scian);
            }
            if (q == '' && inval != undefined && inval != '') {
                q = inval;
            } else if (q == '') {
                q = '*';
            }

            q = encodeURIComponent(q);
            var rU = '/' + q + '?lat=' + position.coords.latitude + '&lng=' + position.coords.longitude + '&scian=' + actividad_scian;

            var baseU = (mob) ? '/m' : '';
            window.location = baseU + '/buscar/lugares-cercanos' + rU;
        }

        var paramU = getAllUrlParams().q;

        if (paramU != undefined && paramU != '') {
            self.serv.then(function () {
                self.selectedItem = querySearch(decodeURIComponent(paramU))[0];
            });
        }
    }
})();

function getAllUrlParams(url) {

    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
    var obj = {};
    if (queryString) {
        queryString = queryString.split('#')[0];
        var arr = queryString.split('&');
        for (var i = 0; i < arr.length; i++) {
            var a = arr[i].split('=');
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function (v) {
                paramNum = v.slice(1, -1);
                return '';
            });
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();
            if (obj[paramName]) {
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                if (typeof paramNum === 'undefined') {
                    obj[paramName].push(paramValue);
                } else {
                    obj[paramName][paramNum] = paramValue;
                }
            } else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
}