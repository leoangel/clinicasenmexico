
var cnt = document.getElementById('comentarios');
var fbcomments = '<span style="text-align:center">Cargando comentarios ...</span><div class="fb-comments" data-numposts="5" width="100%"></div>';

function loadComments() {

    cnt.innerHTML = fbcomments;

    setTimeout(function () {
        FB.XFBML.parse(cnt);
        cnt.querySelector('span').remove();        
    }, 1000);
}

if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', load);
} else {
    load();
}

function load() {
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3&appId=833537917011889&autoLogAppEvents=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}


if ("IntersectionObserver" in window) {

    var eObserver = new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
            if (entry.isIntersecting) {
                if (entry.target == cnt) {
                    eObserver.unobserve(cnt);
                    loadComments();
                }
            }
        });
    });
    if (cnt)
        eObserver.observe(cnt);
} else {
    loadComments();
}



