--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 12rc1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: custom; Type: TABLE; Schema: autocompletar; Owner: postgres
--

CREATE TABLE autocompletar.custom (
    inicial character varying(1),
    nombre character varying(300),
    n_lower character varying(300),
    url character varying(300),
    count integer,
    tabla character varying(50)
);


ALTER TABLE autocompletar.custom OWNER TO postgres;

--
-- Data for Name: custom; Type: TABLE DATA; Schema: autocompletar; Owner: postgres
--

INSERT INTO autocompletar.custom VALUES ('i', 'Idime Bucaramanga', 'idime bucaramanga', 'idime-bucaramanga', 1, 'custom_queries');
INSERT INTO autocompletar.custom VALUES ('i', 'Idime Pereira', 'idime pereira', 'idime-pereira', 1, 'custom_queries');
INSERT INTO autocompletar.custom VALUES ('i', 'Idime Ibague', 'idime ibague', 'idime-ibague', 1, 'custom_queries');
INSERT INTO autocompletar.custom VALUES ('i', 'Idime Manizales', 'idime manizales', 'idime-manizales', 1, 'custom_queries');
INSERT INTO autocompletar.custom VALUES ('i', 'Idime toberin', 'idime toberin', 'idime-toberin', 1, 'custom_queries');
INSERT INTO autocompletar.custom VALUES ('i', 'Idime | Instituto de Diagnostico Medico | Sedes', 'idime | instituto de diagnostico medico | sedes', 'idime', 31, 'custom_queries');


--
-- Name: autoc_custom_count_idx; Type: INDEX; Schema: autocompletar; Owner: postgres
--

CREATE INDEX autoc_custom_count_idx ON autocompletar.custom USING btree (count DESC);


--
-- Name: autoc_custom_gin_trgm_idx; Type: INDEX; Schema: autocompletar; Owner: postgres
--

CREATE INDEX autoc_custom_gin_trgm_idx ON autocompletar.custom USING gin (n_lower public.gin_trgm_ops);


--
-- Name: autoc_custom_inicial_idx; Type: INDEX; Schema: autocompletar; Owner: postgres
--

CREATE INDEX autoc_custom_inicial_idx ON autocompletar.custom USING btree (inicial);


--
-- PostgreSQL database dump complete
--
