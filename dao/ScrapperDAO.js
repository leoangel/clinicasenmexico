var connection = require('../library/db/connection');

var ScrapperDAO = {

    /**
     * Consulta distinct dominios
     */
    getDominios: function () {

        var sql = 'select distinct dominio from sedes where dominio is not null and dominio != \'\'';
        return this.findAll(sql, [], 'getDominios', false);
    },

    /**
     * Util
     */
    findAll: function (sql, params, ref, emptyError = true) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err, ref, 500);
                    }
                    if (emptyError && results.rowCount === 0) {
                        return errFnc('Not found', ref, 404);
                    }
                    fnc(results.rows);
                });
            }
        }
    },
}    

module.exports = ScrapperDAO;