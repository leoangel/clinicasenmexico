var connection = require('../library/db/connection');

var SearchDAO = {

    T_SERVICIOS_REGION: 'servicios_regiones',

    /**
     * Consulta full text 
     * 
     * Si la consulta se intenta de nuevo aumentar el valor de rank para disminuir los resultados
     * y evitar el alto costo de ordenar
     */
    find: function (search, retry = false, location = false) {

        var rank_i = 0.2;

        var sql = 'select * from ( select nombre, url, tipo, descripcion, pre_path_url, ';
        sql += "ts_rank(ts_full_text, plainto_tsquery($1)) r from full_text_search ";
        sql += "where ts_full_text @@ plainto_tsquery($1) ) s ";
        sql += 'where r > ' + rank_i + ' order by r desc limit 25;';

        console.log(sql)

        return this.findAll(sql, [search], 'find', false);
    },

    /**
     * Consultas personalizadas por el usuario
     */
    customQueriesByUrl: function (query) {

        var sql = 'select * from custom_queries where url = $1';

        return {
            then: function (fnc, errFnc) {
                SearchDAO.findOne(sql, [query], 'customQueries').then(function (consulta) {
                    if (consulta.tipo == 'query')
                        SearchDAO.findAll(consulta.sql, [], 'customQueries 2').then(function (results) {
                            fnc(consulta, results);
                        }, errFnc);
                    else
                        fnc(consulta);
                }, errFnc);
            }
        };
    },

    customQueries: function () {

        var sql = 'select * from custom_queries';
        return this.findAll(sql, [], 'customQueries');
    },

    customQueriesSearch: function () {

        var sql = 'select title as nombre, url, description as descripcion from custom_queries';
        return this.findAll(sql, [], 'customQueriesSearch');
    },

    /**
     * Todas las Consultas personalizadas por el usuario
     */
    AllStaticCustomQueries: function () {

        var sql = 'select * from custom_queries where gstatic = \'1\'';
        return this.findAll(sql, [], 'AllStaticCustomQueries');
    },

    findRegionesByServicioByInitial: function (servicio, query) {

        var sql = 'SELECT region_nombre as text, ubi_url as id FROM ' + this.T_SERVICIOS_REGION + ' where serv_url = $1 and region_nombre_l ilike $2 order by count desc';

        console.log(sql);

        return this.findAll(sql, [servicio, query + '%'], 'findRegionesByServicioByInitial');
    },
    findRegionesByServicioBySub: function (servicio, query) {

        var sql = 'SELECT region_nombre as text, ubi_url as id FROM ' + this.T_SERVICIOS_REGION + ' where serv_url = $1 and region_nombre_l ilike $2 order by count desc';
        return this.findAll(sql, [servicio, '%' + query + '%'], 'findRegionesByServicioBySub');
    },

    /**************************************************************************/
    /**
     * Consultas autocompletar
     */
    findServiceByInitial: function (search) {

        var sql = 'select nombre, url, \'s\' t from autocompletar.servicios where inicial = $1 order by count desc limit 5';
        return this.findAll(sql, [search], 'findServiceByInitial', false);
    },

    findEstablishmentByInitial: function (search) {

        var sql = 'select * from (select nombre, url, \'q\' t from autocompletar.custom where inicial = $1 limit 5) t';
        sql += ' union all ';
        sql += 'select * from (select nombre, url, \'e\' t from autocompletar.establecimientos where inicial = $1 order by count desc limit 5) e';

        return this.findAll(sql, [search], 'findEstablishmentByInitial', false);
    },

    findLocationByInitial: function (search) {

        var sql = 'select nombre, url, \'u\' t from autocompletar.municipios_barrios where inicial = $1 order by count desc limit 25';
        return this.findAll(sql, [search], 'findLocationByInitial', false);
    },
    findServiceBySub: function (search) {

        var sql = 'select nombre, url, \'s\' t from autocompletar.servicios where n_lower ilike lower(unaccent($1)) order by count desc limit 5';
        return this.findAll(sql, ['%' + search + '%'], 'findServiceBySub', false);
    },

    findEstablishmentBySub: function (search) {

        var sql = 'select * from (select nombre, url, \'q\' t from autocompletar.custom where n_lower ilike lower(unaccent($1)) limit 5) t';
        sql += ' union all ';
        sql += 'select * from (select nombre, url, \'e\' t from autocompletar.establecimientos where n_lower ilike lower(unaccent($1)) limit 5) e';
        return this.findAll(sql, ['%' + search + '%'], 'findEstablishmentBySub', false);
    },

    findLocationBySub: function (search) {

        var sql = 'select nombre, url, \'u\' t from autocompletar.municipios_barrios where n_lower ilike lower(unaccent($1)) order by count desc limit 25';
        return this.findAll(sql, ['%' + search + '%'], 'findLocationBySub', false);
    },
    findAllServices: function () {

        var sql = 'SELECT nombre, url, \'s\' t FROM autocompletar.servicios';
        return this.findAll(sql, [], 'findAllServices');
    },
    findServiceLevenshtein: function (search) {

        var sql = 'SELECT nombre, url, \'s\' t, levenshtein(nombre, lower(unaccent($1))) lv FROM autocompletar.servicios order by lv limit 6';
        return this.findAll(sql, [search], 'findServiceLevenshtein');
    },
    findLocationLevenshtein: function (search) {

        var inicial = search.substring(0, 1).toLowerCase();

        var sql = 'SELECT nombre, url, \'u\' t, levenshtein(n_lower, lower(unaccent($1))) lv FROM autocompletar.municipios_barrios where inicial = $2 order by lv limit 15';
        return this.findAll(sql, [search, inicial], 'findLocationLevenshtein');
    },

    findAll: function (sql, params, ref, emptyError = true) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {

                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (emptyError && results.rowCount === 0) {
                        return errFnc('Not found', ref);
                    }
                    fnc(results.rows);
                });
            }
        }
    },

    findOne: function (sql, params, ref) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (results.rowCount === 0) {
                        return errFnc('Not found', ref);
                    }
                    fnc(results.rows[0], results.rows);
                });
            }
        }
    },

    initcap: function (a = '') {
        return a.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
            return m.toUpperCase();
        });
    }

};

module.exports = SearchDAO;
