var connection = require('../library/db/connection');

var SitemapDAO = {

    getAllServices: function () {

        var sql = "select * from especialidades"; 

        return this.findAll(sql, [], 'getAllServices');
    },    

    getAllRegiones: function () {

        var sql = "select * from municipios_barrios"; 

        return this.findAll(sql, [], 'getAllServices');
    },  

    getAllRegionesServicios: function () {

        var sql = "select serv_url, ubi_url reg_url from servicios_regiones"; 

        return this.findAll(sql, [], 'getAllServices');
    },  

    getAllRegionesServicios: function () {

        var sql = "select serv_url, ubi_url reg_url from servicios_regiones"; 

        return this.findAll(sql, [], 'getAllServices');
    },  

    getAllEstablecimientos: function () {

        var sql = "select nombre_url from sedes"; 

        return this.findAll(sql, [], 'getAllServices');
    },  

    findAll: function (sql, params, ref, emptyError = true) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err + ' params: ' + params.toString(), ref, 500);
                    }
                    if (emptyError && results.rowCount === 0) {
                        return errFnc('Not found' + ' params: ' + params.toString(), ref, 404);
                    }
                    fnc(results.rows);
                });
            }
        }

    },

    findOne: function (sql, params, ref) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (results.rowCount === 0) {
                        return errFnc('Not found', ref);
                    }
                    fnc(results.rows[0], results.rows);
                });
            }
        }
    },    

};

module.exports = SitemapDAO;

