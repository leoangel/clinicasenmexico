var connection = require('../library/db/connection');

var DirectorioDAO = {

    PAGE_SIZE: 20,
    
    /**
     * Tablas
     */
    T_ESTABLECIMIENTO : 'sedes',

    T_REGIONES : 'municipios',
    T_CIUDADES_PRINCIPALES : 'ciudades_principales',
    T_SUBREGIONES : 'municipios_barrios',

    T_SERVICIOS : 'especialidades',
    
    // Servicio * (Regiones|Subregiones) : (Ciudades|Barrios|Subregiones)
    T_SERVICIOS_REGION : 'servicios_regiones',
    T_ESTABLECIMIENTO_SERVICIOS : 'servicios',


    findPopulares: function () {

        var sql = "select title, case when pre_path_url is null then '/directorio' else '/' || pre_path_url end || '/' || url url, description from custom_queries" 
        sql += " union all "
        sql += " select title, url, description from ( "
        sql += " select serv_nombre title, '/especialidad/' || serv_url url, serv_nombre description from especialidades order by count desc limit 9"
        sql += " ) t ";

        return this.findAll(sql, [], 'findPopulares');
    },    
    /**
     * Obtiene informacion detalle
     */
    findEstablecimiento: function (nombre) {

        var sql = 'select * from ' + this.T_ESTABLECIMIENTO + ' where nombre_url = $1 or nombre_sede_url = $2';
        return this.findOne(sql, [nombre, nombre], 'findEstablecimiento');
    },
    findEstablecimientoServicio: function (sede, servicio) {

        var sql = 'select * from servicios_sedes where sede_nombre_url = $1 and serv_url = $2';
        return this.findAll(sql, [sede, servicio], 'findEstablecimientoServicio');
    },
    /**
     * Consulta subregiones por servicio
     */
    findRegionesPorServicio: function (servicio) {

        var sql = 'SELECT * FROM ' + this.T_SERVICIOS_REGION + ' where serv_url = $1 and t = \'r\' order by municipio';
        return this.findAll(sql, [servicio], 'findRegionesPorServicio');
    },         
    /**
     * Consulta Actividad principal
     */
    getServicios: function () {

        var sql = 'SELECT * FROM ' + this.T_SERVICIOS + ' order by serv_nombre';
        return this.findAll(sql, [], 'getServicios');
    },
    /**
     * Consulta Municipios, P = '' => ciudades importantes, inicial => inicial*, all => *
     */
    getRegiones: function (p = '') {

        var sql = '';
        var params = [];

        if(p == 'all')
            sql = 'SELECT * FROM ' + this.T_REGIONES + ' order by region_url';
        if(p == '')
            sql = 'SELECT * FROM ' + this.T_CIUDADES_PRINCIPALES + ' order by count desc';
        else if(sql == '') {
            sql = 'SELECT * FROM ' + this.T_REGIONES + ' where inicial = $1 order by region_url';    
            params = [p.toLowerCase()];
        }
        
        return this.findAll(sql, params, 'getRegiones', false);
    },
    /**
     * Obtiene establecimientos por servicio
     */
    findServicioPage: function (servicio, page) {

        var sql1 = 'select * from ' + this.T_SERVICIOS + ' where serv_url = $1';
        var sql2 = 'select * from ' + this.T_ESTABLECIMIENTO_SERVICIOS + ' where serv_url = $1 and pag > $offset ORDER BY pag ASC limit ' + this.PAGE_SIZE;

        return DirectorioDAO.paginacion(sql1, sql2, [servicio], page, 'findServicioPage')
    },
    /**
     * Consulta Est por Region
     */
    findRegionPage: function (municipio, page) {

        var sql1 = 'select * from ' + this.T_REGIONES + ' where region_url = $1';
        var sql2 = 'select * from ' + this.T_ESTABLECIMIENTO + ' where region_url = $1 and pag_reg > $offset ORDER BY pag_reg ASC limit ' + this.PAGE_SIZE;

        return DirectorioDAO.paginacion(sql1, sql2, [municipio], page, 'findRegionPage')
    },
    /**
     * Consulta Servicios por region
     */
    findServiciosByRegion: function (region) {

        var sql = "select *, case when t = 's' then barrio || ', ' || municipio || ', ' || departamento when t = 'r' then municipio || ', ' || departamento end region_nombre  from " + this.T_SERVICIOS_REGION + " where ubi_url = $1 order by serv_nombre";

        return this.findAll(sql, [region], 'findServiciosByRegion');
    },	
    /**
     * Consulta Est por Region y Subregion
     */
    findRegionSubregionPage: function (municipio, page) {

        var sql1 = 'select * from ' + this.T_SUBREGIONES + ' where url = $1';
        var sql2 = 'select * from ' + this.T_ESTABLECIMIENTO + ' where #col_ubi_url = $1 and #col_page > $offset ORDER BY #col_page ASC limit ' + this.PAGE_SIZE;

        var cols = {    
            s : { ubi_url : 'barrio_url', pag : 'pag_subreg'}, 
            r : { ubi_url : 'region_url', pag : 'pag_reg'}
        };

        return DirectorioDAO.paginacionUbicacion(sql1, sql2, [municipio], page, cols, 'findRegionSubregionPage')
    },

    /**
     * Consulta Est por Ubicacion y Servicio
     */
    findPorServicioRegion: function (servicio, region, page) {

        var sql1 = 'select * from ' + this.T_SERVICIOS_REGION + ' where serv_url = $1 and ubi_url = $2';
        var sql2 = 'select * from ' + this.T_ESTABLECIMIENTO_SERVICIOS + ' where serv_url = $1 and #col_ubi_url = $2 and #col_page > $offset ORDER BY #col_page ASC limit ' + this.PAGE_SIZE;

        var cols = {    
            s : { ubi_url : 'barrio_url', pag : 'pag_serv_subreg'}, 
            r : { ubi_url : 'region_url', pag : 'pag_serv_reg'}
        };

        return DirectorioDAO.paginacionUbicacion(sql1, sql2, [servicio, region], page, cols, 'findPorServicioRegion')
    },      
    
    /**
     * Hace aleatorio la primera pagina si hay mas de 1 pagina
     * 
     * @param {type} count
     * @returns {DirectorioDAO.calcularPaginaRandom.result|DirectorioDAO.calcularPaginaRandom.size|Number}
     */
    calcularPaginaRandom: function (count) {

        if (typeof count == 'string')
            count = parseInt(count);

        if (this.PAGE_SIZE >= count)
            return 0;

        var size = this.PAGE_SIZE / 2;
        var max = count / size;
        var result = size * (Math.floor(Math.random() * max));

        if (result != 0 && result % this.PAGE_SIZE == 0)
            result = result + size;

        if (result + this.PAGE_SIZE > count)
            result = count - this.PAGE_SIZE;

        return result;
    },

    findAll: function (sql, params, ref, emptyError = true) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err + ' params: ' + params.toString(), ref, 500);
                    }
                    if (emptyError && results.rowCount === 0) {
                        return errFnc('Not found' + ' params: ' + params.toString(), ref, 404);
                    }
                    fnc(results.rows);
                });
            }
        }

    },

    findOne: function (sql, params, ref) {
        return {
            then: function (fnc, errFnc) {

                connection.getClient().query(sql, params, function (err, results) {
                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (results.rowCount === 0) {
                        return errFnc('Not found', ref);
                    }
                    fnc(results.rows[0], results.rows);
                });
            }
        }
    },

    /**
     * Genera la paginacion
     * 
     * @param {type} fnc
     * @param {type} errFnc
     * @param {type} sql1
     * @param {type} sql2
     * @param {type} params
     * @param {type} page
     * @param {type} ref
     * @returns {undefined}
     */
    paginacion: function (sql1, sql2, params, page, ref) {

        return {
            then: function (fnc, errFnc) {

                if (isNaN(page) || page < 1) {
                    page = 1;
                }
                page = page - 1;

                connection.getClient().query(sql1, params, function (err, data1) {
                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (data1.rowCount === 0) {
                        return errFnc('Not found Paginacion row params: ' + params.toString(), ref);
                    }
                    var dataRow = data1.rows[0];
                    var offset = (page === 0) ? DirectorioDAO.calcularPaginaRandom(dataRow.count) : DirectorioDAO.PAGE_SIZE * page;

                    sql2 = sql2.replace('$offset', offset);
                    connection.getClient().query(sql2, params, function (err, results) {
                        if (err) {
                            return errFnc(err, ref);
                        }
                        if (results.rowCount === 0) {
                            return errFnc('Not found Paginacion data params: ' + params.toString(), ref);
                        }

                        var pages = Math.ceil(dataRow.count / DirectorioDAO.PAGE_SIZE);
                        fnc(results.rows, results.rowCount, dataRow, page + 1, pages);
                    });
                });

            }
        }
    },
    
    paginacionUbicacion: function (sql1, sql2, params, page, cols, ref) {

        return {
            then: function (fnc, errFnc) {

                if (isNaN(page) || page < 1) {
                    page = 1;
                }
                page = page - 1;

                connection.getClient().query(sql1, params, function (err, data1) {
                    if (err) {
                        return errFnc(err, ref);
                    }
                    if (data1.rowCount === 0) {
                        return errFnc('Not found Paginacion servub row params: ' + params.toString(), ref);
                    }
                    var dataRow = data1.rows[0];
                    var offset = (page === 0) ? DirectorioDAO.calcularPaginaRandom(dataRow.count) : DirectorioDAO.PAGE_SIZE * page;

                    sql2 = sql2.replace(new RegExp('#col_ubi_url', 'g'), cols[dataRow.t]['ubi_url']);
                    sql2 = sql2.replace(new RegExp('#col_page', 'g'), cols[dataRow.t]['pag']);
                    sql2 = sql2.replace('$offset', offset);
                    
                    connection.getClient().query(sql2, params, function (err, results) {
                        if (err) {
                            return errFnc(err, ref);
                        }
                        if (results.rowCount === 0) {
                            return errFnc('Not found Paginacion servub data params: ' + params.toString(), ref);
                        }

                        var pages = Math.ceil(dataRow.count / DirectorioDAO.PAGE_SIZE);
                        fnc(results.rows, results.rowCount, dataRow, page + 1, pages);
                    });
                });

            }
        }
    }

};

module.exports = DirectorioDAO;

