var errorLog = require('./errorlog');
var mobileDetect = require('mobile-detect');
var config = require('../../config/app');


var UtilFncs = function (req, res) {

    this.req = req;
    this.res = res;

    this.deviceVersion = null;

    /**
     * Devuelve string con la ubicacion de la vista 
     */
    this.getViewDevice = function (view, prefix = null) {

        let folderDevice = this.getWebVersion();
        folderDevice = (prefix) ? prefix + '/' + folderDevice : folderDevice;

        return folderDevice + '/' + view;
    }

    this.getWebVersion = function () {

        if (this.deviceVersion) return this.deviceVersion;

        if (this.req.get('mobile-version'))
            this.deviceVersion = this.req.get('mobile-version')
        else
            this.deviceVersion = (this.isMobile()) ? 'mobile' : 'desk';

        return this.deviceVersion;
    }

    // Identifica si es mobile o desktop
    this.isMobile = function () {
        var agent = new mobileDetect(this.req.headers['user-agent']);
        if (agent.mobile())
            return true;
        else
            return false;
    }

    this.isCanonical = function () {
        return (this.req.query.a) ? true : false;
    }
    /**
     * Devuelve la url base para usar en rel= 
     */
    this.getBaseUrl = function () {
        return config.url[this.getWebVersion()]
    }

    this.getConfig = function (param) {
        if (param)
            return config[param]
        else
            return config
    }

    this.getUrls = function () {

        let base = config.url[this.getWebVersion()];
        let original = this.req.originalUrl;
        let urls = {
            desk: config.url.desk + original,
            mobile: config.url.mobile + original,
            amp: config.url.amp + original,
            current: original,          // path eje: /directorio/idime o /ips/nombre-sede
            fullUrl: base + original,   // Actual eje: https://clinicas.com/directorio/idime o https://amp.clinicas.com/directorio/idime
            base: base,                 // Actual https://clinicas.com o https://amp.clinicas.com
            baseDesk: config.url.desk,
            baseMobile: config.url.mobile,
            baseAmp: config.url.amp
        }
        return urls;
    }

    this.getCustomQueriesPath = function () {
        return config.custom_queries_path;
    }
    /**
     * Render view, agrega partials
     */
    this.render = function (view, data = {}, defaultPartials = true, prefix) {

        view = this.getViewDevice(view, prefix);
        let pathPartials = 'partials/' + this.getWebVersion() + '/';

        if (defaultPartials)
            data.partials = { head: pathPartials + 'head-page', header: pathPartials + 'header-page', footer: pathPartials + 'footer-page' };
        else
            data.partials = { head: pathPartials + 'head-home', header: pathPartials + 'header-home', footer: pathPartials + 'footer-home' };

        if (data.canonical == undefined)
            data.canonical = (this.req.query.a) ? true : false;

        if(this.getWebVersion() == 'desk'){
            data.partials.right_ads = pathPartials + 'right-ads'
        }

        data.url = this.getUrls();

        this.res.render(view, data);
    }

    this.getFirstWord = function (query = '') {
        return query.split(' ')[0];
    }

    this.isInitial = function (text) {
        return text.length == 1
    }

    /**
     * Enviar estado y vista 404
     */

    /**
     * Registra el error, y envia un 404
     */
    this.fncError = function (err, origen) {
        errorLog.error(err, origen);
    }

    this.errorSend404 = function (err, origen) {
        this.fncError(err, origen);
        this.res.status(404).render('error/404');
    }

    this.json = function (data) {
        this.res.json(data);
    }

    this.jsonError = function (err, origen) {
        this.fncError(err, origen);
        this.res.status(500).json();
    }

    /**
     * Solo para string
     */
    this.isEmpty = function (v = '') {

        if(typeof v == 'object'){            
                return true;      
        }

        if (v == '' || v == undefined || v.replace(' ', '') == '')
            return true;
        else
            return false;
    }

    /**
     * Para string, evitar errores y ataques HPP
     */
    this.getReqQuery = function(param, decode = true){
        let p = this.req.query[param];		
        let r = (typeof p == 'object')? p[0] : p;

	if(this.isEmpty(r))
	    return '';

        if(decode)
            return decodeURIComponent(r.replace(/\+/g, " "))
        else 
            return r;
    }

    /**
     * Retorna informacion sobre la paginacion
     * 
     */
    this.getInfoPage = function (pages, page) {
        var pageData = {
            hasPages: (pages > 1) ? true : false,
            current: page,
            pages: pages
        };

        if (page > 1) {
            pageData.breadcrumbs = page;
            pageData.prev = page - 1;
        } else {
            pageData.first = true;
        }
        if (page - 1 > 1) {
            pageData.prev1 = page - 2;
        }
        if (page < pages) {
            pageData.next = page + 1;
        }
        if (page + 1 < pages) {
            pageData.next1 = page + 2;
        }
        return pageData;
    }
}


var Util = {
    init: function (req, res) {
        return new UtilFncs(req, res);
    }
};

module.exports = Util;