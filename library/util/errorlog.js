var fs = require('fs')
var logsPath = __dirname + '/../../storage/logs/';

var errorLog = {
    notFound: function (req) {

	var message = '[' + getCurrentDate() + '] ' + req.connection.remoteAddress + ': ' + req.url + '\r\n';
	fs.appendFile(logsPath + '404.log', message, function (err, data) {
	    if (err) {
		return console.log(err);
	    }
	});
    },
    error: function (err, origen) {

	var errMessage = '';

	if (typeof err == 'object' && err != null) {
	    if (err.hasOwnProperty('message')) {
		errMessage = err.message;
	    } else {
		errMessage = 'Error desconocido';
	    }
	} else {
	    errMessage = err;
	}
	if (origen !== undefined) {
	    errMessage = origen + ' > ' + errMessage;
	}
	var message = '[' + getCurrentDate() + '] ' + errMessage + '\r\n';
	fs.appendFile(logsPath + 'error.log', message, function (err, data) {
	    if (err) {
		return console.log(err);
	    }
	});
    },
    denue: function (consulta, lat, lng, err) {

	var errMessage = '';

	if (typeof err == 'object') {
	    errMessage = JSON.stringify(err);
	} else {
	    errMessage = err;
	}

	var message = '[' + getCurrentDate() + '] ' + consulta + ' lat:' + lat + ' lng:' + lng + ' : ' + errMessage + '\r\n';
	fs.appendFile(logsPath + 'error-denue.log', message, function (err, data) {
	    if (err) {
		return console.log(err);
	    }
	});
    },
    redirect: function (req, urlde) {

	var message = '[' + getCurrentDate() + '] ' + req.connection.remoteAddress + ': ' + req.url + ' :: redirect:  ' + urlde + '\r\n';
	fs.appendFile(logsPath + '301.log', message, function (err, data) {
	    if (err) {
		return console.log(err);
	    }
	});
    },

    webshot: function (err = 'Indefinido', domain, path, id_registro) {

	var message = '[' + getCurrentDate() + '] ' + err + ' :: Domain: ' + domain + ' :: Path:  ' + path  + ' :: Id:  ' + id_registro + '\r\n';
	fs.appendFile(logsPath + 'webshot.log', message, function (err, data) {
	    if (err) {
		return console.log(err);
	    }
	});
    }
}


function getCurrentDate() {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    var hh = today.getHours();
    var ii = today.getMinutes();
    var ss = today.getSeconds();

    if (dd < 10) {
	dd = '0' + dd
    }

    if (mm < 10) {
	mm = '0' + mm
    }

    if (hh < 10) {
	hh = '0' + hh
    }

    if (ii < 10) {
	ii = '0' + ii
    }

    if (ss < 10) {
	ss = '0' + ss
    }

    return yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + ii + ':' + ss;
}

module.exports = errorLog;