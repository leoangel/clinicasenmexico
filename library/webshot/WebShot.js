var webshot = require('node-webshot');
var errorLog = require('../util/errorlog');

var Controller = {

    test: function (req, res) {

	res.send('Generando');

	captureImage('www.patolab.co', getPath('mexicoo', 'desktop'), getOptionsDevice('desktop'));	
	captureImage('www.patolab.co', getPath('mexicoo', 'mobile'), getOptionsDevice('mobile'));	

    }
}

function captureImage(domain, path, options, id = null) {
    webshot(domain, path, options, function (err) {
	if(err)
	    errorLog.webshot(err, domain, path, id);
    });
}

function getOptionsDevice(device = 'desktop') {

    var options = {};

    options.mobile = {
	quality: 25,
	renderDelay: 1000,
	screenSize: {
	    width: 360
	    , height: 480
	}
	, shotSize: {
	    width: 360
	    , height: 720
	}
	, userAgent: 'Mozilla/5.0 (Linux; Android 7.0; SM-G930V Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36'
    };
    
    options.desktop = {
	quality: 25,
	renderDelay: 1000,
	screenSize: {
	    width: 1024
	    , height: 768
	}
	, shotSize: {
	    width: 1024
	    , height: 1152
	}
	, userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36'
    };
    
    return options[device];
}

function getPath(name, device) {
    return '/projects/pruebas/webshot/images/' + name + '-' + device + '.png';
}

module.exports = Controller;