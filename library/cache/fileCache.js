var crypto = require('crypto');
var fs = require('fs');
var log = require('../util/errorlog.js');

var FileCache = {
    DIR_CACHE: __dirname + '/../../storage/cache',
    /**
     * 
     * @param {type} key
     * @returns {Boolean}
     */
    has: function (key, callback) {
	this.__isFile(this.__getFileName(key), callback);
    },
    /**
     * 
     * @param {type} key
     * @param {type} value
     * @returns {undefined}
     */
    put: function (key, value) {
	this.__saveFile(this.__getFileName(key), value);
    },
    /**
     * 
     * @param {type} key
     * @returns {undefined}
     */
    get: function (key, callback) {
	this.__getFile(this.__getFileName(key), callback);
    },
    __isFile: function (fileName, callback) {
	fs.stat(this.__getPathFileCache(fileName), function (err, stat) {
	    if (err) {
		return callback();
	    }
	    if (stat && stat.isFile())
		return callback(true);
	});
    },
    __saveFile: function (fileName, content) {

	var contentString = '';
	if (typeof content == 'object') {
	    contentString = JSON.stringify(content);
	}
	
	var pathFileCache = this.__getPathFileCache(fileName);
	this.__isFile(fileName, function (exists) {
	    if (exists)
		log.error('Ya existe el archivo de cache: ' + exists);
	    fs.writeFileSync(pathFileCache, contentString);
	});
    },
    __getFile: function (fileName, callback) {
	fs.readFile(this.__getPathFileCache(fileName), 'utf8', function (err, data) {
	    if (err)
		log.error(err);
	    callback(JSON.parse(data));
	});
    },
    __getFileName: function (key) {
	var hash = crypto.createHash('md5').update(key).digest('hex');
	return hash + '.cache';
    },
    __getPathFileCache: function (fileName) {
	return this.DIR_CACHE + '/' + fileName;
    }
}


module.exports = FileCache;