
var dao = require('../../dao/ScrapperDAO');
var util = require('../util/UtilFncs');
var extract = require('./ExtractInfo');
var download = require('./Download');
var config = require('../../config/scrapper');
var fs = require('fs');
var regx = require('../util/Regex')

var Scrapper = {

    test2: function (req, res) {

//        const wget = require('wget-improved');
//
//        const src = 'https://maps.guiamexico.mx/img/archivo.jpg?lng=-106.41975102&lat=23.24';
//        const output = '/projects/pruebas/images/prueba.jpg';
//
//        async function descargar() {
//            let promesa = (src, out, x) => {
//                return new Promise(function (resolve, reject) {
//                    setTimeout(() => {
//                        let download = wget.download(src, out);
////                        download.on('start', x => console.log('Start'));
//                        download.on('end', () => resolve(x));
//                        download.on('error', () => reject(x));
//                    }, x * 500);
//                });
//            }
//
//            var p1 = promesa(src, output, 1);
//            var p2 = promesa(src, output, 2);
//            var p3 = promesa(src, output, 3);
//
//            try {
//                let r = await Promise.all([p1, p2, p3]);
//                console.log(r);
//            } catch (e) {
//                console.log(e);
//            }
//        }
//
//        descargar();

//        Promise.all([p1, p2, p3]).then(x => console.log(`Ok ${x}`)).catch(e => console.log(`Fail ${e}`));
//        var re = Promise.all(promesas).then(x => console.log(x)).catch(e => console.log(e));
//        console.log(re);

        // promise(src, output).then(x => console.log(x)).catch(e => console.log(e));

        res.send('test descarga');
    },

    test: function (req, res) {

        /**
         * TODO 
         * 
         * REGEX images/btn-servicios.png marca como url
         * modificar el promise all por ejecutar las promesas una a una para evitar detener las demas
         * descargas
         */
        console.log(regx.isURL2('images/btn-servicios.png'));
        console.log(regx.isURL2('images/btnservicios.png'));
        console.log(regx.isURL2('/images/btnservicios'));
        console.log(regx.isURL2('domain.com'));
        console.log(regx.isURL2('domain.co'));
        console.log(regx.isURL2('https://domain.co'));
        console.log(regx.isURL2('http://domain.co'));
        console.log(regx.isURL2('http://as.domain.co'));
        return;

        dao.getDominios().then(function (domains) {
            domains.forEach(function (data) {

                let protocol = 'http://';
                let domain = data.dominio;
                let path = config.path + '/' + domain

                domain = domain.toLowerCase();
//                console.log('Scrapper: ' + domain);
                
                if (!fs.existsSync(path)) {
                    fs.mkdirSync(path);
                }

                var fullUrl = protocol + domain;
                if(!fullUrl.match(regx.domain())){
                    console.log('Error url: ' + fullUrl);
                    return;
                }

                let descargar = new download(fullUrl, path, config.delay);
                extract.start(fullUrl, (metadata, images) => {

                    var asyncDescarga = async () => {

                        var promisesImg = images.map((x, y) => {
                            let output = path + '/' + x.getFileName();
                            descargar.start(x.src, output, y)
                        });

                        try {
                            let r = await Promise.all(promisesImg);
                        } catch (e) {
                            console.log('Descargando error: ' + e);
                        }
                    }
                    asyncDescarga();

                });
                
            });
        }, function (err, origen) {
            util.errorSend404(err, origen);
        });

        res.send('Iniciando scrapper');
    }
}

module.exports = Scrapper;