
var regex = require('../util/Regex');

var Image = function() {
    
    this.alt = null;
    this.src = null;
    
    this.getFileName = () => {
        return this.src.match(regex.fileNameFromUrl());
    }
}

module.exports = Image;

