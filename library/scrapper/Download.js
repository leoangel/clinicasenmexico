const wget = require('wget-improved');

/* 
 * uri
 * name file
 * path file
 */

var regex = require('../util/Regex');

var Download = class {
    constructor(host, path, delay) {
        this.domain = host;
        this.path = path;
        this.delay = delay;
    }
    start(p_src, out, x) {

        var src = this.getUri(p_src);
        var delay = this.delay;
        
        var dominio = this.domain;
        
        return new Promise(function (resolve, reject) {
            setTimeout(() => {
                try {
                    let download = wget.download(src, out);
                    download.on('end', () => {
                        console.log('Descargando: ' + src);
                        console.log('Output     : ' + src);
                        resolve(out)
                    });
                    download.on('error', () => {
                        console.log('Descargando: ' + src);
                        console.log('Output     : ' + src);
                        reject(out)
                    });
                } catch (e) {
                    console.log('Error al descargar domain: ' + dominio);
                    console.log('Error al descargar src: ' + p_src);
                    console.log('Error al descargar uri: ' + src);
                    console.log('Error: ' + e);
                }
            }, x * delay);
        });
    }

    /**
     * Obtiene la url para descargar ajustando si es una url parcial 
     */
    getUri(url) {

        // Si la url contiene el dominio si no es una url relativa
        let uri = null;
        if (regex.isURL(url)) {
            uri = url;
        } else {
            uri = (url.charAt(0) == '/') ? url : '/' + url;
            uri = this.domain + uri;
        }

        console.log(this.domain);
        console.log(uri);
        console.log(uri);

        return uri;
    }
}


module.exports = Download;

