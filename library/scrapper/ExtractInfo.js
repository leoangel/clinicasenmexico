const rp = require('request-promise');
const cheerio = require('cheerio');
const download = require('./Download');

const metaInfo = require('./MetaInfoStructure');
const imageInfo = require('./ImageStructure');


/* 
 * Meta data
 * Images
 */
class ScrapWeb {
        
    constructor(url) {
        this.url = url;
    }
    
    init() {
        return rp(this.getCheerioOptions(this.url));
    }
    
    setCheerio($) {
        this.cheerio = $;
    }
    
    getMetadata() {
        
        let $ = this.cheerio;
        
        metaInfo.title = $('title').text();
        metaInfo.fbimg = $('meta[property="og:image"]').attr('content');
        metaInfo.description = $('meta[name=description]').attr('content');
        metaInfo.keywords = $('meta[name=keywords]').attr('content');
        
        return metaInfo;
    }
    
    getImages() {
        
        let $ = this.cheerio;
        
        let images = [];
        
        $('img').each(function (x, y) {
            let data = new imageInfo();
            data.alt = $(y).attr('alt');
            data.src = $(y).attr('src');
            images.push(data);
        });
        
        return images;
    }
    
    getCheerioOptions (url) {
        this.url = url;
        return {
            uri: url,
            insecure: true,
            transform: function (body) {
                return cheerio.load(body);
            }
        };
    }
    
}

var Extract = {
    start: async function (dominio, callback) {
        try {
            let scrap = new ScrapWeb(dominio);
            let $ = await scrap.init();
            
            scrap.setCheerio($);
            let metadata = scrap.getMetadata();
            let images = scrap.getImages();
            // Download images
            
            callback(metadata, images, null);
            
        } catch (e) {
            console.log('Error: ' + e);
            console.log(dominio);
        }
    }
}

module.exports = Extract;