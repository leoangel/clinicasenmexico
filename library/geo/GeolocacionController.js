var errorLog = require('../library/util/errorlog');
var denue = require('inegi-denue');
var dao = require('../dao/DirectorioDAO');
var request = require("request");

var client = denue.createClient();

var GeolocacionController = {
    actualizar: function (req, res, next) {
	dao.getDirecciones().then(function (direcciones) {

	    console.log(direcciones.length + ' Direcciones encontradas');

	    var url = '';
	    var contador = 1;

	    direcciones.forEach(function (direccion) {

		setTimeout(function () {

		    url = 'https://nominatim.openstreetmap.org/search?q=' + encodeURI(direccion.direccion) + '&countrycodes=co&format=json&polygon=0&addressdetails=1&bounded=0&limit=1&email=nessmash@gmail.com';

		    request.get({url: url}, function (e, r, body) {
			if (e) {
			    console.log(e);
			    registrarError(direccion.id, direccion.direccion);
			} else {

			    if (isJson(body)) {
				var json = JSON.parse(body);

				if (json.length !== 0) {
				    console.log(url);
				    registrar(json, direccion);
				} else {
				    url = 'https://nominatim.openstreetmap.org/search?city=' + encodeURI(direccion.municipio) + '&state=' + encodeURI(direccion.departamento) + '&countrycodes=co&format=json&polygon=0&addressdetails=1&bounded=0&limit=1&email=nessmash@gmail.com';
				    console.log(url);
				    request.get({url: url}, function (e, r, body) {
					if (e) {
					    console.log(e);
					    registrarError(direccion.id, direccion.direccion);
					} else {
					    var json = JSON.parse(body);

					    if (json.length !== 0) {
						registrar(json, direccion);
					    } else {
						registrarError(direccion.id, direccion.direccion);
//                                                registrarError(direccion.direccion);
					    }
					}
				    });
				}
			    } else {
				console.log(url);
				registrarError(direccion.direccion);
			    }
			}
		    });

		}, 4000 * contador++);

	    });

	}, function (err, origen) {
	    errorLog.error(err, origen);
	    res.status(404).end();
	});

	res.send('Iniciando Geolocalizacion ...');
    }
}

function registrar(json, direccion) {
    var lat = json[0].lat;
    var lon = json[0].lon;

    dao.guardarLocalizacion(direccion.municipio, direccion.departamento, lat, lon);
    console.log('Registrando Geolocalizacion ' + direccion.direccion + ': ' + lat + ', ' + lon);
}

function registrarError(id, direccion) {
    dao.guardarError(id);
    console.log('Error localizacion ' + direccion);
}

function isJson(str) {
    try {
	JSON.parse(str);
    } catch (e) {
	return false;
    }
    return true;
}

module.exports = GeolocacionController;

