var path = require('path');

var config = {
    
    path: path.join(__dirname + '/..', 'data/scrapper'),
    
    // Tiempo espera por cada peticion para no saturar el servidor
    delay: 500,
}

module.exports = config;