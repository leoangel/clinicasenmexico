
var path = require('path');

var config = {
    
    port: '3111',
    
    env: 'development',
    
    // Urls !!No agregar / al final
    url: {
        desk: 'https://clinicasyhospitales.local',
        mobile: 'https://clinicasyhospitales.local',
        amp: 'https://amp.clinicasyhospitales.local'
    },
    
    custom_queries_path: 'directorio',
    
    version: {
        localStorage: "1.1.2"
    },

    /**
     * View
     */
    view_cache: false,
    view_engine: 'hjs',    
    view_path: path.join(__dirname + '/..', 'views'),
    
    
};

module.exports = config;


