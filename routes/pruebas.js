var express = require('express');
var router = express.Router();

var WebShot = require('../library/webshot/WebShot');
var Scrapper = require('../library/scrapper/Scrapper');

router.get('/webshot', WebShot.test);
router.get('/scrapper', Scrapper.test);


module.exports = router;