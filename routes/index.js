var express = require('express');
var router = express.Router();

var config = require('../config/app');

/**
 * Version de los archivos usados en localStorage 
 * ayuda a limpiar el localstorage si la actualizacion se actualiza
 */
router.get('/json/update-version', function (req, res) {
    res.json({ version: config.version.localStorage });
});

/******************************************************************************/
/**
 * Consultas personalizadas, buscar y Autocompletar
 */
var urlsC = [
    { url: '/buscar', controller: 'buscar' },
    { url: '/json/autoc/search', controller: 'jsonAutocompletar' },
    { url: '/json/regiones/:servicio', controller: 'jsonRegionesPorServicio' },
];

var SearchController = require('../controllers/SearchController');

urlsC.forEach(function (data) {
    router.get(data.url, SearchController[data.controller]);
});

/******************************************************************************/
/**
 * Consultas personalizadas
 */
var urlsCQ = [
    { url: '/directorio/:consulta', controller: 'customQueries' },
    { url: '/consultas', controller: 'consultasPopulares' },
    { url: '/json/consultas', controller: 'jsonConsultas' },
    { url: '/coronavirus/:consulta', controller: 'customQueries' },
    // { url: '/custom-queries/' + config.custom_queries_path + '/:consulta', controller: 'customQueries' },
    // { url: '/customqueries/generate/views/static-files', controller: 'generateStaticView' },
];

var CustomQueriesController = require('../controllers/CustomQueriesController');

urlsCQ.forEach(function (data) {
    router.get(data.url, CustomQueriesController[data.controller]);
});

/******************************************************************************/
/**
 * Contacto
 */
var ContactController = require('../controllers/ContactController');

router.get('/contacto', ContactController.getForm);
router.post('/contacto', ContactController.sendMessage);


/******************************************************************************/
/**
 * Directorio
 */

var directorioController = require('../controllers/DirectorioController');

/**
 * Urls
 */
var urls = [
    { url: '/', controller: 'getHome' },

    { url: '/politica-privacidad', controller: 'getPoliticaPrivacidad' },
    { url: '/cookies', controller: 'getPoliticaCookies' },

    { url: '/ips', redirect: '/' },
    { url: '/ips/:ips', controller: 'getEstablecimientoODirectorio' },
    { url: '/ips/:ips/:servicio', controller: 'getEstabServicio' },

    { url: '/especialidad', controller: 'getServicios', },
    { url: '/especialidades', controller: 'getServicios' },
    { url: '/especialidad/:servicio/municipios', controller: 'getServicioRegiones', },
    { url: '/especialidad/:servicio', controller: 'getServicioPage', page: 'servicio' },

    { url: '/municipios', controller: 'getRegiones' },
    { url: '/:region', controller: 'getEstPorRegion', page: 'region' },
    { url: '/:region/servicios-medicos', controller: 'getServiciosPorRegion' },
    { url: '/:region/:servicio', controller: 'getEstPorRegionServicio', page: ['region', 'servicio'] },
];


/**
 * Itera y asigna las urls
 */
urls.forEach(function (data) {

    if (data.controller) {
        router.get(data.url, directorioController[data.controller]);
    }

    if (data.page) {
        router.get(data.url + '/page/:page', function (req, res, next) {
            if (isNaN(req.params.page) || req.params.page < 2) {
                res.redirect(301, getUrlFromParams(req, data.page, data.url));
            } else {
                directorioController[data.controller](req, res, next);
            }
        });
        router.get(data.url + '/page', function (req, res, next) {
            res.redirect(301, getUrlFromParams(req, data.page, data.url));
        });
    }

    if (data.redirect)
        router.get(data.url, function (req, res, next) {
            res.redirect(301, data.redirect);
        });
});

/**
 * Funcion para obtener los parametros de las urls amigables
 * 
 * @param {*} req 
 * @param {*} params 
 * @param {*} urlreq 
 */
var getUrlFromParams = function (req, params, urlreq) {

    if (params instanceof Array) {
        params.forEach(function (p) {
            urlreq = urlreq.replace(':' + p, req.params[p]);
        });
    } else {
        urlreq = urlreq.replace(':' + params, req.params[params]);
    }

    return urlreq;
}

module.exports = router;