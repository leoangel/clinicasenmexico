var express = require('express');
var router = express.Router();

/******************************************************************************/
/**
 * Sitemaps
 */

var SitemapController = require('../controllers/SitemapController');

router.get('/servicios', function (req, res, next) {
   SitemapController.generarServicios(req, res, next);
});
router.get('/regiones', function (req, res, next) {
   SitemapController.generarRegiones(req, res, next);
});
router.get('/regiones-servicios', function (req, res, next) {
   SitemapController.generarRegionesServicios(req, res, next);
});
router.get('/establecimientos', function (req, res, next) {
   SitemapController.generarEstablecimientos(req, res, next);
});

module.exports = router;