var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config/app.js');
var routes = require('./routes/index');
var helmet = require('helmet');

var errorlog = require('./library/util/errorlog.js');

var app = express();

// view engine setup
app.set('port', config.port);
app.set('env', config.env);
app.set('views', config.view_path);
app.set('view engine', config.view_engine);
app.set('view cache', config.view_cache);


app.use(helmet.hidePoweredBy());
app.use(helmet.xssFilter());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(logger("combined"));

if (config.env === 'development') {
    var pruebas = require('./routes/pruebas');
    app.use('/pruebas', pruebas);

    var sitemaps = require('./routes/sitemaps');
    app.use('/generar/sitemaps', sitemaps);
}

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    errorlog.notFound(req);
    var err = new Error('Not Found');
    err.status = 404;
    res.status(err.status);
    res.render('error/' + err.status);
});

// error handlers

// development error handler
// will print stacktrace
if (config.env === 'development') {
    app.use(function (err, req, res, next) {
        errorlog.error(err.message, req.originalUrl);
        res.status(err.status || 500);
        res.render('error/error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    errorlog.error(err, req.originalUrl);
//    res.status(err.status || 500);
//    res.render('error/error', {
//        message: err.message,
//        error: {}
//    });
});


module.exports = app;
